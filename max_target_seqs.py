#!/usr/bin/env python

from __future__ import print_function

import argparse


def max_target_seqs(fh, num=1, key=None, ecol=10):
	acceptable = {}
	
	def get_evalue(line): return float(line.split('\t')[ecol])

	for l in fh:
		if l.startswith('#'): continue
		elif not l.strip(): continue
		l = l.replace('\n', '')
		sl = l.split('\t')
		if len(sl) < ecol: continue

		if key is None: k = tuple(sl[:2])
		else: k = sl[key-1]

		if k not in acceptable: acceptable[k] = [l]
		elif len(acceptable[k]) < num: acceptable[k].append(l)
		else:
			acceptable[k].sort(key=get_evalue)
			if get_evalue(acceptable[k][-1]) > get_evalue(l): acceptable[k][-1] = l
	return acceptable

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('-n', type=int, default=1, help='Number of HSPs to retain (default: 1)')
	parser.add_argument('-k', type=int, default=None, help='1-indexed column to enforce uniqueness on (default:first two)')
	parser.add_argument('-e', type=int, default=11, help='1-indexed column containing E-values')
	parser.add_argument('infile', nargs='*', default=['/dev/stdin'], help='Blast TSVs to read in')

	args = parser.parse_args()

	for fn in args.infile:
		with open(fn) as fh:
			accepted = max_target_seqs(fh, args.n, key=args.k, ecol=args.e-1)
			for k in sorted(accepted):
				for hsp in accepted[k]: print(hsp)
