#!/usr/bin/env python 
import argparse
import sys

from kevtools_common.types import TCIDCollection


def main(args):
	parser = argparse.ArgumentParser()
	parser.add_argument('-l', '--level', type=int, default=3, help='What level to simplify to')
	parser.add_argument('tcids', nargs='*', help='TC-IDs to simplify. Leave blank to read from stdin')

	params = parser.parse_args(args)

	targets = TCIDCollection()
	if not params.tcids:
		with open('/dev/stdin') as f:
			for l in f: targets.add_tcid(l.strip())
	else:
		for tcstr in params.tcids:
			targets.add_tcid(tcstr.strip())

	simplified = TCIDCollection()
	for tcid in targets: simplified.add_tcid(tcid[:params.level])

	for tcid in sorted(simplified): print(tcid)

if __name__ == '__main__':
	main(sys.argv[1:])

