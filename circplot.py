#!/usr/bin/env python
from __future__ import print_function, division

import os
import matplotlib
import sys
if __name__ == '__main__':
	if '-q' in sys.argv: backend = os.environ.get('MPLBACKEND', 'Agg')
	elif '-o' in sys.argv: backend = os.environ.get('MPLBACKEND', 'Agg')
	else: backend = os.environ.get('MPLBACKEND', 'Qt4Agg')
matplotlib.use(backend)
INITBACKEND = backend
import matplotlib.pyplot as plt
import matplotlib.cm
import numpy as np

import argparse
import tempfile
import subprocess

from Bio import SeqIO
from Bio.Blast import NCBIXML

def info(*things): print('[INFO]', *things, file=sys.stderr)

class Selection(object):
	def __init__(self, xspan, yspan, color=None):
		self.xspan = xspan
		self.yspan = yspan
		self.artists = {'polar':[], 'rectangular':[]}
		self.view_center = 0.
		self._x = None
		self.color = (0.0, 0.5, 1.0, 0.5) if color is None else color

	def draw_polar(self, ax):
		pos = 2 * np.pi * (self.xspan[0] - self.view_center)
		width = 2 * np.pi * (self.xspan[1] - self.xspan[0])
		bottom = self.yspan[0]
		height = self.yspan[1] - self.yspan[0]
		fc = self.color
		self.artists['polar'].append(ax.bar(pos, height=height, width=width, bottom=bottom, fc=self.color, zorder=1.2))
		for rect in self.artists['polar'][-1]: 
			rect.set_picker(True)
			rect.selection = self

	def draw_rect(self, ax):
		pos = self.xspan[0] - self.view_center
		width = (self.xspan[1] - self.xspan[0])
		bottom = self.yspan[0]
		height = self.yspan[1] - self.yspan[0]
		fc = self.color
		self.artists['rectangular'].append(ax.bar(pos, height=height, width=width, bottom=bottom, fc=self.color, zorder=1.2))
		for rect in self.artists['rectangular'][-1]: 
			rect.selection = self
			rect.set_picker(True)

	def resize(self, newval, index=1, axis=0):
		for rect in self.artists['polar'][-1]:
			if axis == 0:
				if index == 0:
					newpos = newval*2*np.pi - rect.get_x()
					newwidth = rect.get_width() - newval*2*np.pi
				else:
					newpos = rect.get_x()
					newwidth = newval*2*np.pi - newpos
				rect.set_x(newpos)
				rect.set_width(newwidth)
			else: raise NotImplementedError
		for rect in self.artists['rectangular'][-1]:
			if axis == 0:
				if index == 0:
					newpos = newval - rect.get_x()
					newwidth = rect.get_width()
				else:
					newpos = rect.get_x()
					newwidth = newval - newpos
				rect.set_x(newpos)
				rect.set_width(newwidth)
			else: raise NotImplementedError

	def remove(self):
		for coord in self.artists:
			for artist in self.artists[coord]:
				artist.remove()
		del self

	def update_view(self):
		offset = 0
		for artist in self.artists['polar']:
			if isinstance(artist, matplotlib.container.BarContainer):
				for rect in artist.get_children():
					if self._x is None: self._x = rect.get_x()
					oldpos = rect.get_x()
					rect.set_x(self._x - self.view_center)
					offset = rect.get_x() - oldpos
			elif isinstance(artist, matplotlib.patches.FancyArrow):
				#artist.set_x(self._x - self.view_center)
				verts = artist.get_xy()
				verts[:,0] += offset
		for artist in self.artists['rectangular']:
			if isinstance(artist, matplotlib.container.BarContainer):
				for rect in artist.get_children():
					rect.set_x(((self._x - self.view_center) % (2*np.pi)) / (2*np.pi))
			elif isinstance(artist, matplotlib.patches.FancyArrow):
				#artist.set_x(self._x - self.view_center)
				verts = artist.get_xy()
				verts[:,0] = (verts[:,0] + offset/2/np.pi) % 1

class HSP(object):
	def __init__(self, query, subject, qspan, sspan, qlen, slen, expect, length, ident, qseq):
		self.query = query
		self.subject = subject
		self.qspan = qspan
		self.sspan = sspan
		self.qlen = qlen
		self.slen = slen
		self.expect = expect
		self.length = length
		self.ident = ident
		self.genome = None
		self.color = None
		self.qseq = qseq

		self.artists = {'polar':[], 'rectangular':[]}

		self.view_center = 0.
		self._x = None

	def __str__(self):
		return 'HSP({}-{}/{}, {}-{}/{})'.format(self.qspan[0], self.qspan[1], self.qlen, self.sspan[0], self.sspan[1], self.slen)

	def get_summary_line(self):
		return '{} ({}-{} / {}) vs. {} ({}-{} / {}): {:0.0%}ident., expect={:0.2f}'.format(
			self.query,
			self.qspan[0], self.qspan[1],
			self.qlen,
			self.subject,
			self.sspan[0], self.sspan[1],
			self.slen,
			self.ident,
			self.expect
		)

	def __lt__(self, other):
		if min(self.sspan) < min(other.sspan): return True
		else: return False
		#if self.expect < other.expect: return True
		#elif self.expect > other.expect: return False
		#elif self.ident > other.ident: return True
		#else: return False

	def draw_polar(self, ax):
		pos = 2*np.pi * (self.sspan[1] + self.sspan[0])/2 / self.slen - self.view_center
		width = 2 * np.pi * (self.sspan[1] - self.sspan[0]) / self.slen
		bottom = self.genome.inner_rad
		height = self.genome.outer_rad - self.genome.inner_rad
		percentforward = 0.7 * np.pi * 0.01 * np.sign(self.sspan[1]-self.sspan[0]) / self.genome.outer_rad
		#if sspan[1] > sspan[0]: fc = 'red'
		#else: fc = 'cyan'
		#return ax.bar(0, height=self.outer_rad-self.inner_rad, width=2*np.pi, bottom=self.inner_rad, fc=self.color)
		alpha = self.ident * 0.8
		self.artists['polar'].append(ax.bar(pos, height=height, width=width, bottom=bottom, fc=self.color, zorder=1.1, alpha=alpha))
		for bar in self.artists['polar'][-1]: 
			bar.set_picker(True)
			bar.hsp = self

		self.artists['polar'].append(ax.arrow(self.sspan[0]*2*np.pi/self.slen - self.view_center, self.genome.inner_rad + (self.genome.outer_rad - self.genome.inner_rad) * .5, percentforward/5, 0, head_width=0.1, head_length=2*abs(percentforward), fc=(0,0,0,alpha), ec=(0,0,0,alpha**.5)))
		self.artists['polar'][-1].set_picker(True)
		self.artists['polar'][-1].hsp = self

		#return self.artists['polar'][-1]

	def draw_rect(self, ax):
		pos = (self.sspan[0] + self.sspan[1])/2 / self.slen - self.view_center
		width = (self.sspan[1] - self.sspan[0]) / self.slen
		bottom = self.genome.inner_rad
		height = self.genome.outer_rad - self.genome.inner_rad
		percentforward = 0.7 * 0.01 * np.sign(self.sspan[1] - self.sspan[0]) / np.pi

		alpha = self.ident * 0.8

		self.artists['rectangular'].append(ax.bar(pos, height=height, width=width, bottom=bottom, fc=self.color, zorder=1.1, alpha=alpha))
		for bar in self.artists['rectangular'][-1]: 
			bar.set_picker(True)
			bar.hsp = self

		self.artists['rectangular'].append(ax.arrow(self.sspan[0]/self.slen, (self.genome.outer_rad + self.genome.inner_rad) * 0.5, percentforward/5, 0, head_width=0.1, head_length=2*abs(percentforward), fc=(0,0,0,alpha), ec=(0,0,0,alpha**.5)))
		self.artists['rectangular'][-1].set_picker(True)
		self.artists['rectangular'][-1].hsp = self


		#return self.artists['rectangular'][-1]

	def is_in(self, selelist):
		for rect in self.artists['rectangular'][0].get_children():
			selfxspan = [rect.get_x(), rect.get_width() + rect.get_x()]
			selfyspan = [self.genome.inner_rad, self.genome.outer_rad]
			for sele in selelist:
				for selerect in sele.artists['rectangular'][0].get_children():

					seleyspan = sorted([selerect.get_y(), selerect.get_y() + selerect.get_height()])
					if not ((seleyspan[0] <= selfyspan[0]) and (selfyspan[1] <= seleyspan[1])): continue
					selexspan = sorted([selerect.get_x(), selerect.get_x() + selerect.get_width()])
					if ((selexspan[0] <= selfxspan[0]) and (selfxspan[1] <= selexspan[1])): return True
			return False


	def update_view(self):
		offset = 0
		for artist in self.artists['polar']:
			if isinstance(artist, matplotlib.container.BarContainer):
				for rect in artist.get_children():
					if self._x is None: self._x = rect.get_x()
					oldpos = rect.get_x()
					rect.set_x(self._x - self.view_center)
					offset = rect.get_x() - oldpos
			elif isinstance(artist, matplotlib.patches.FancyArrow):
				#artist.set_x(self._x - self.view_center)
				verts = artist.get_xy()
				verts[:,0] += offset
		for artist in self.artists['rectangular']:
			if isinstance(artist, matplotlib.container.BarContainer):
				for rect in artist.get_children():
					rect.set_x(((self._x - self.view_center) % (2*np.pi)) / (2*np.pi))
			elif isinstance(artist, matplotlib.patches.FancyArrow):
				#artist.set_x(self._x - self.view_center)
				verts = artist.get_xy()
				verts[:,0] = (verts[:,0] + offset/2/np.pi) % 1

class Genome(object):
	def __init__(self, name, inner_rad=None, outer_rad=None, length=1, color=None, description=''):
		self.inner_rad = inner_rad
		self.outer_rad = outer_rad
		self.name = name
		self.color = color
		self.length = length
		self.description = description

		self.artists = []

		self.view_center = 3*np.pi/2

	def __hash__(self):
		return hash(tuple((self.name, self.length, self.description)))

	def __lt__(self, other): 
		if self.name < other.name: return True
		elif self.name > other.name: return False
		elif self.length < other.length: return True
		else: return False

	def draw_polar(self, ax):
		self.artists.append(ax.bar(0, height=self.outer_rad-self.inner_rad, width=2*np.pi, bottom=self.inner_rad, fc=self.color))
		return self.artists[-1]

	def draw_rect(self, ax):
		self.artists.append(ax.bar(0.5, height=self.outer_rad-self.inner_rad, width=1, bottom=self.inner_rad, fc=self.color))
		return self.artists[-1]

class Circplot(object):
	mode = 'normal'
	fig = None
	axcirc = None
	axrect = None
	def __init__(self, width=7, height=7, outfile_seq=None):
		self.width = width
		self.height = height
		self.outfile_seq = outfile_seq

		self.hsps = []
		self.genomes = []
		self.queries = []

		self.entities = []
		self.entdict = {}

		self.innermost = 2
		self.genomewidth = 0.7
		self.genomestep = 1

		self.cmap = matplotlib.cm.get_cmap('tab10')

		self.mousedata = {}

	def _initialize(self):
		self.fig = plt.figure()

		self.fig.set_figwidth(self.width)
		self.fig.set_figheight(self.height)
		self.fig.set_tight_layout(True)
	
		self.gs = self.fig.add_gridspec(4, 1)
		self.axcirc = self.fig.add_subplot(self.gs[:3,0], projection='polar')
		self.axrect = self.fig.add_subplot(self.gs[3,0])

		self.fig.canvas.mpl_connect('pick_event', self.onpick)
		self.fig.canvas.mpl_connect('button_press_event', self.onmousedown)
		self.fig.canvas.mpl_connect('key_press_event', self.onkeydown)
		self.fig.canvas.mpl_connect('button_release_event', self.onmouseup)
		self.fig.canvas.mpl_connect('motion_notify_event', self.onmousemove)
		self.fig.canvas.mpl_connect('scroll_event', self.onscroll)

		self.axcirc.set_thetagrids(tuple())
		self.axcirc.set_rgrids(tuple())
		self.axcirc.grid(False)
		self.axrect.set_xlim([0, 1])
		#self.axcirc.set_rlim([0, 10])

		self.axrect.xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, _: '{:.0%}'.format(x%2)))

		self.render()
		self.axcirc.set_rmin(0)
		self.update_title()
		plt.show()

	def update_title(self):
		if self.mode == 'normal': self.fig.canvas.set_window_title('NORMAL mode')
		elif self.mode.startswith('visualo'): self.fig.canvas.set_window_title('VISUAL-OBJECT mode')
		elif self.mode.startswith('visual'): self.fig.canvas.set_window_title('VISUAL mode')
		elif self.mode.startswith('zoom'): self.fig.canvas.set_window_title('ZOOM mode')
		elif self.mode.startswith('pan'): self.fig.canvas.set_window_title('PAN mode')
		elif self.mode.startswith('moveo'): self.fig.canvas.set_window_title('MOVE-OBJECT mode')
		elif self.mode.startswith('move'): self.fig.canvas.set_window_title('MOVE mode')
		elif self.mode.startswith('deselect'): self.fig.canvas.set_window_title('DESELECT mode')
		else: self.fig.canvas.set_window_title('UNDEFINED mode')

	def add_query(self, name, length=1, color=None):
		if color is None: color = 'k'
		if isinstance(name, Genome):
			name.color = color if name.color is None else color
			self.queries.append(name)
		else:
			genome = Genome(name=name, length=length, color=color)
			self.queries.append(genome)

	def add_genome(self, name, length=1, color=None): 
			
		if not self.genomes: 
			inner_rad = self.innermost
			outer_rad = inner_rad + self.genomewidth
		else: 
			inner_rad = self.genomes[-1].inner_rad + self.genomestep
			outer_rad = self.genomes[-1].inner_rad + self.genomestep + self.genomewidth


		if color is None: color = self.cmap((len(self.genomes)%10) / 10)

		if isinstance(name, Genome):
			name.inner_rad = inner_rad if name.inner_rad is None else name.inner_rad
			name.outer_rad = outer_rad if name.outer_rad is None else name.outer_rad
			name.color = color if name.color is None else color
			self.genomes.append(name)
			self.entities.append(name)
		else:
			genome = Genome(inner_rad=inner_rad, outer_rad=outer_rad, name=name, length=length, color=color)
			self.genomes.append(genome)
			self.entities.append(genome)

	def get_genome_by_r(self, r):
		''' returns only the FIRST genome for a given r '''
		for genome in self.genomes:
			if genome.inner_rad <= r <= genome.outer_rad: return genome
		return None

	def print_qfrags(self):
		qstrs = {}
		for hsp in self.hsps:
			if 'selection' in self.entdict and not hsp.is_in(self.entdict['selection']): continue
			if hsp.genome not in qstrs: qstrs[hsp.genome] = []
			qstrs[hsp.genome].append(hsp)

		genomes = set()
		nhsps = 0
		printme = ''
		for genome in sorted(qstrs):
			printme += '# Found {} HSPs for {}:\n'.format(len(qstrs[genome]), genome.description)
			genomes.add(genome)
			for hsp in sorted(qstrs[genome]): 
				nhsps += 1
				printme += '>{} Expect {:0.1e}, ident {:0.0%}, q({}-{}/{}) s({}-{}/{})\n'.format(
					hsp.query.split()[0],
					hsp.expect,
					hsp.ident,
					hsp.qspan[0], hsp.qspan[1],
					hsp.qlen,
					hsp.sspan[0], hsp.sspan[1],
					hsp.slen
				)
			printme += '\n'
		info('Found {} fragments from {} genome(s)'.format(nhsps, len(genomes)))
		print(printme)

	def write_qfrags(self):
		qstr = ''
		n = 0
		genomes = set()
		for hsp in self.hsps:
			if 'selection' in self.entdict and not hsp.is_in(self.entdict['selection']): continue
			qstr += '>{}_{}-{}\n{}\n'.format(hsp.query, hsp.qspan[0], hsp.qspan[1], hsp.qseq)
			genomes.add(hsp.genome)
			n += 1
		qstr = qstr.strip()

		fn = 'qfrag_{:x}.fnt'.format(abs(hash(qstr))) if self.outfile_seq is None else self.outfile_seq
		with open(fn, 'w') as f: f.write(qstr)
		info('Wrote {} fragments from {} genome(s) to {}'.format(n, len(genomes), fn))

	def add_hsp(self, hsp): 
		self.hsps.append(hsp)
		self.entities.append(hsp)

	def render(self):
		for e in self.entities: 
			if 'draw_polar' in dir(e): e.draw_polar(self.axcirc)
			if 'draw_rect' in dir(e): e.draw_rect(self.axrect)
		self.fig.canvas.draw()

	def run(self):
		self._initialize()

	def onpick(self, event): 
		if 'hsp' in dir(event.artist): 
			if not self.mode.startswith('normal'): return
			print(event.artist.hsp.get_summary_line())
		elif (self.mode == 'deselect') and ('selection' in dir(event.artist)):
			if 'selection' in self.entdict:
				event.artist.selection.remove()
				self.entdict['selection'].remove(event.artist.selection)
				del event.artist.selection
				#event.artist.remove()
				self.fig.canvas.draw()

				if not self.entdict['selection']: del self.entdict['selection']

	def onmousedown(self, event):
		if not event.inaxes: return
		elif self.mode == 'move':

			self.mode = 'movedrag'

			self.mousedata['axes'] = event.inaxes
			self.mousedata['startpos'] = np.array([event.xdata, event.ydata])
			self.mousedata['lastpos'] = np.array([event.xdata, event.ydata])

		elif self.mode == 'moveo':
			genome = self.get_genome_by_r(event.ydata)
			if not genome: return

			self.mode = 'moveodrag'

			self.mousedata['axes'] = event.inaxes
			self.mousedata['startpos'] = np.array([event.xdata, event.ydata])
			self.mousedata['lastpos'] = np.array([event.xdata, event.ydata])
			self.mousedata['genome'] = genome

		elif self.mode.startswith('visual'):
			if self.mode.startswith('visualo'):
				genome = self.get_genome_by_r(event.ydata)
				if not genome: return
				yspan = [genome.inner_rad, genome.outer_rad]
			else: yspan = [self.innermost, self.axcirc.get_ylim()[1]]
			self.mode = 'visualdrag'

			if 'selection' not in self.entdict: self.entdict['selection'] = []
			self.mousedata['axes'] = event.inaxes
			self.mousedata['startpos'] = np.array([event.xdata, event.ydata])
			self.mousedata['lastpos'] = np.array([event.xdata, event.ydata])

			oldrlim = self.axcirc.get_ylim()
			oldylim = self.axrect.get_ylim()
			if event.inaxes is self.axcirc: pos = event.xdata/2/np.pi
			else: pos = event.xdata

			e = Selection([pos, pos+0.006], yspan)
			self.entdict['selection'].append(e)
			e.draw_polar(self.axcirc)
			e.draw_rect(self.axrect)
			self.axcirc.set_rlim(oldrlim)
			self.axrect.set_ylim(oldylim)

			self.fig.canvas.draw()

	def print_help(self):
		print('?: Show this help page')
		print('ESC: Return to normal mode')
		print('c: Calculate coverages')
		print('d: Enter deselect mode')
		print('dd: Deselect all')
		print('h: Print information on HSPs')
		print('m: Enter move mode')
		print('M: Enter move-object mode')
		print('v: Enter visual selection mode')
		print('V: Enter visual object-specific selection mode')
		print('w: Write selected query fragments to disk (WIP, writes all to file for now)')

	def onkeydown(self, event): 
		if event.key == '?':
			self.print_help()
		elif self.mode.endswith('drag'): return
		elif event.key == 'escape':
			self.mode = 'normal'
			self.update_title()
		elif event.key == 'c':
			cov = self.get_coverages()
		elif event.key == 'd':
			if self.mode == 'deselect': 
				self.deselect()
				self.mode = 'normal'
				self.update_title()
				return
			self.mode = 'deselect'
			self.update_title()
		elif event.key == 'h':
			self.print_qfrags()
		elif event.key == 'M':
			self.mode = 'moveo'
			self.update_title()
		elif event.key == 'm':
			self.mode = 'move'
			self.update_title()
		elif event.key == 'v':
			self.mode = 'visual'
			self.update_title()
		elif event.key == 'V':
			self.mode = 'visualo'
			self.update_title()
		elif event.key == 'w':
			self.write_qfrags()
	def onmouseup(self, event): 
		if self.mode.startswith('movedrag'):
			self.mode = 'move'
		if self.mode.startswith('moveodrag'):
			self.mode = 'moveo'
			del self.mousedata['genome']
		if self.mode.startswith('visualdrag'):
			self.mode = 'visual'
	def onmousemove(self, event): 
		if not event.inaxes: return 

		###PANNING###
		elif self.mode.startswith('movedrag') or self.mode.startswith('moveodrag'):
			if event.inaxes != self.mousedata['axes']: return

			pos = np.array([event.xdata, event.ydata])

			if event.inaxes in (self.axcirc, self.axrect):
				if event.button == 1:
					#pan
					if event.inaxes is self.axcirc:
						truedisplacement = self._handle_polar_wraparound(self.mousedata['lastpos'], pos)

						rlim = self.axcirc.get_rmin(), self.axcirc.get_rmax()
						####self.axcirc.set_rlim([rlim[0] - truedisplacement[1], rlim[1] - truedisplacement[1]])
					else: 
						truedisplacement = self._handle_rectangular_wraparound(self.mousedata['lastpos'], pos)


					#encapsulate!
					self.apply_offset(-truedisplacement[0], genome=self.mousedata.get('genome', None))
					
				elif event.button == 3:
					#zoom
					truedisplacement = self._handle_polar_wraparound(self.mousedata['lastpos'], pos)
					
					rlim = self.axcirc.get_rmin(), self.axcirc.get_rmax()
					self.axcirc.set_rlim([rlim[0] + truedisplacement[1]/2, rlim[1] - truedisplacement[1]/2])

				self.fig.canvas.draw()

			self.mousedata['lastpos'] = pos
		###VISUAL SELECTION###
		elif self.mode.startswith('visualdrag'):
			if event.inaxes != self.mousedata['axes']: return

			#TODO: resizeability on other dimensions

			pos = event.xdata/2/np.pi if event.inaxes is self.axcirc else event.xdata
			self.entdict['selection'][-1].resize(pos, index=1, axis=0)

			self.fig.canvas.draw()

		else: return
	def onscroll(self, event): pass

	def apply_offset(self, offset, genome=None):
		for hsp in self.hsps: 
			#if self.mode.startswith('moveodrag') and hsp.genome != self.mousedata['genome']: continue
			if genome is not None and hsp.genome != genome: continue
			hsp.view_center += offset
			hsp.update_view()
		if 'selection' in self.entdict:
			for sele in self.entdict['selection']:
				sele.view_center += offset
				sele.update_view()
	def deselect(self, index=None):
		if 'selection' not in self.entdict: return
		if index is None:
			for ent in self.entdict['selection']: ent.remove()
			del self.entdict['selection']
		else: raise NotImplementedError
		self.fig.canvas.draw()

	def get_coverages(self):
		qstrs = {}
		for hsp in self.hsps:
			if 'selection' in self.entdict and not hsp.is_in(self.entdict['selection']): continue
			if hsp.genome not in qstrs: qstrs[hsp.genome] = []
			qstrs[hsp.genome].append(hsp)

		#coverages = {'queries':{}, 'subjects':{}}

		genomes = set()
		alnres = {}
		for query in sorted(self.queries):
			alnres[query] = set()

		printmeq = ''
		printmeq += '# Query sequence coverages\n'
		printmes = ''
		printmes += '# Subject sequence coverages\n'

		queries = set()
		for genome in sorted(qstrs):
			alnres[genome] = set()
			for hsp in qstrs[genome]:
				alnres[genome].update(set(range(min(hsp.sspan), max(hsp.sspan)+1)))
				if self.queries:
					for q in self.queries:
						if hsp.query == q.description: 
							query = q
							queries.add(q)
							break
					alnres[query].update(set(range(min(hsp.qspan), max(hsp.qspan)+1)))

			printmes += ('>{} {}/{} ({:0.2%})\n'.format(genome.name, len(alnres[genome]), genome.length, len(alnres[genome])/genome.length))

		for q in sorted(queries):
			printmeq += '>{} {}/{} ({:0.2%})\n'.format(q.name, len(alnres[q]), q.length, len(alnres[q])/q.length)

		print(printmeq + printmes)

	def _handle_polar_wraparound(self, pos1, pos2, threshold=np.pi/2):
		displacement = pos2 - pos1

		#case 1: clockwise >0 to <2pi: disp -= 2pi
		if (0 <= pos1[0] <= threshold) and (2*np.pi - threshold <= pos2[0] <= 2*np.pi): displacement[0] -= 2*np.pi
		#case 2: widdershins <2pi to >0: disp += 2pi
		elif (0 <= pos2[0] <= threshold) and (2*np.pi - threshold <= pos1[0] <= 2*np.pi): displacement[0] += 2*np.pi
		#case 3: either direction but beyond threshold
		else: pass
		return displacement

	def _handle_rectangular_wraparound(self, pos1, pos2, threshold=np.pi/2):
		pos2_dilated = pos1 + (pos2 - pos1) * 2 * np.pi
		return self._handle_polar_wraparound(pos1, pos2_dilated, threshold)

def blast(fn1, fn2, evalue=None, length=None, ident=None, tblastn=False):
	f = tempfile.NamedTemporaryFile()

	if tblastn: cmd = ['tblastn']
	else: cmd = ['blastn']

	cmd.extend(['-outfmt', '5', '-query', fn1, '-subject', fn2, '-out', f.name])
	#if evalue is not None: cmd += ['-evalue', str(evalue)]
	
	subprocess.call(cmd)
	f.flush()
	f.seek(0)

	allhsps = {}

	for record in NCBIXML.parse(f):
		#print(record.query, record.query_length)
		#allhsps[record.query] = {}
	
		for alignment in record.alignments:
			#print(alignment.hit_def, alignment.length)
			#allhsps[record.query][alignment.hit_def] = []

			for hsp in alignment.hsps:
				if (evalue is not None) and (hsp.expect > evalue): continue
				if (length is not None) and (hsp.align_length < length): continue
				if (ident is not None) and ((hsp.identities/hsp.align_length) < ident): continue

				if alignment.hit_def not in allhsps: allhsps[alignment.hit_def] = {}
				if record.query not in allhsps[alignment.hit_def]: allhsps[alignment.hit_def][record.query] = []
				allhsps[alignment.hit_def][record.query].append(HSP(record.query, alignment.hit_def, 
					(hsp.query_start, hsp.query_end),
					(hsp.sbjct_start, hsp.sbjct_end),
					record.query_length,
					alignment.length,
					hsp.expect, hsp.align_length, hsp.identities/hsp.align_length,
					hsp.query))
				
				#print('q', hsp.query_start/record.query_length, hsp.query_end/record.query_length)
				#print('s', hsp.sbjct_start/alignment.length, hsp.sbjct_end/alignment.length)
	return allhsps

def luminance(colortup): return np.dot([0.2126, 0.7152, 0.0722, 0.0], colortup)
def get_best_contrast(bg):
	if luminance(bg) < (0x77/0xff): return 'w'
	else: return 'k'

def axcspan(ax, span, length, radius, width, offset=90, counterclock=True, ec=None, fc=None):
	if counterclock: startangle = offset + 360 * (span[0]) / length
	else: startangle = offset - 360 * (span[0]) / length


	print([span[1]-span[0], length-(span[1]-span[0])])
	return ax.pie([span[1]-span[0], length-(span[1]-span[0])], colors=[fc, (1., 0., 1., 0.)], radius=radius, startangle=startangle, counterclock=counterclock, wedgeprops={'width':width, 'ec':ec})

def paxcspan(ax, span, length, radius, width, offset=90, counterclock=True, ec=None, fc=None, zorder=None):
	if counterclock: 
		startangle = offset*np.pi/180 + (span[0]/length)*2*np.pi
		widthsign = 1
	else: 
		startangle = offset*np.pi/180 - (span[0]/length)*2*np.pi
		widthsign = -1

	#print([span[1]-span[0], length-(span[1]-span[0])])
	return ax.bar(startangle, height=width, width=widthsign*(span[1]-span[0])/length*np.pi*2, bottom=radius, zorder=zorder, ec=ec, fc=fc)

class PolarWheelplot(object):
	''' DEPRECATED '''
	transientents = {}
	transientdisp = {}
	mode = 'normal'
	fig = None
	angularlist = []
	def __init__(self, allhsps, fig, offset=90, direction=-1, interval=0.1, rspacing=1, ringwidth=0.7):
		self.allhsps = allhsps
		self.fig = fig

		if direction > 0: self.counterclock = True
		else: self.counterclock = False

		gs = fig.add_gridspec(4, 1)
		self.axpol = fig.add_subplot(gs[:3,0], projection='polar')
		self.axrec = fig.add_subplot(gs[3,0])


		self.axpol.figure.canvas.mpl_connect('pick_event', self.onpick)
		self.axpol.figure.canvas.mpl_connect('button_press_event', self.onmousedown)
		self.axpol.figure.canvas.mpl_connect('key_press_event', self.onkeydown)
		self.axpol.figure.canvas.mpl_connect('button_release_event', self.onmouseup)
		self.axpol.figure.canvas.mpl_connect('motion_notify_event', self.onmousemove)
		self.axpol.figure.canvas.mpl_connect('scroll_event', self.onscroll)

		self.offset = offset
		self.interval = interval
		self.rspacing = rspacing
		self.ringwidth = ringwidth

		self.axpol.set_thetagrids(tuple())
		self.axpol.set_rgrids(tuple())
		self.axpol.grid(False)
		self.axrec.set_xlim([0, 2*np.pi])

		#self.axpol.set_theta_offset(self.offset*np.pi/180)
		#if self.counterclock: self.axpol.set_theta_direction(1)
		#else: self.axpol.set_theta_direction(-1)
		#print(dir(self.axpol))

	def update_rectplot(self, rect):
		print(rect)
		pass

	def main(self):
		radius = 2
		radius = 0
		wedges = set()
		genomecm = matplotlib.cm.get_cmap('tab10')
		self.fig.canvas.set_window_title('NORMAL mode')
		for subjectid, subject in enumerate(self.allhsps):
			print((subjectid%10)/10)
			color = genomecm((subjectid%10)/10)
			fg = get_best_contrast(color)

			radius += self.rspacing
			#ax.annotate(subject.split()[0], (offset*np.pi/180, radius-ringwidth/2), ha='left', va='center', color=fg)
			self.axpol.annotate(subject.split()[0], (self.offset*np.pi/180, radius+self.ringwidth/2), ha='left', va='center', color=fg)


			for queryid, query in enumerate(sorted(self.allhsps[subject])):
				for hsp in self.allhsps[subject][query]:
					barlist = paxcspan(self.axpol, hsp.sspan, hsp.slen, radius, self.ringwidth/2, self.offset, self.counterclock, ec=(0., 0., 0., 0.), fc='red', zorder=1.1)
					for bar in barlist: 
						bar.hsp = hsp
						bar.set_picker(20.0)
					#barlist = paxcspan(self.axrec, hsp.sspan, hsp.slen, radius, self.ringwidth/2, self.offset, self.counterclock, ec=(0., 0., 0., 0.), fc='red', zorder=1.1)
					barlist = paxcspan(self.axrec, hsp.sspan, hsp.slen, radius, self.ringwidth/2, self.offset, self.counterclock, ec=(0., 0., 0., 0.), fc='red', zorder=1.1)
					for bar in barlist: 
						bar.hsp = hsp
						bar.set_picker(20.0)

			barlist = self.axpol.bar(self.offset*np.pi/180, self.ringwidth, width=2*np.pi, bottom=radius, fc=color)
			barlist = self.axrec.bar(self.offset*np.pi/180, self.ringwidth, width=2*np.pi, bottom=radius, fc=color)

		if self.counterclock: ha = 'right'
		else: ha = 'left'
		self.axpol.annotate('ori', (self.offset*np.pi/180, radius+self.rspacing), ha=ha, va='bottom')

		#radius += self.rspacing
		self.axpol.arrow(self.offset*np.pi/180, radius+self.rspacing, (self.counterclock-0.5)*0.1, 0, head_width=0.1, fc='k')
		self.axpol.axvline(self.offset*np.pi/180, color='k', zorder=1.15)
		self.axrec.axvline(self.offset*np.pi/180, color='k', zorder=1.15)

		self.axpol.set_rlim([0, radius+self.rspacing])
		self.axrec.set_xlim([-self.offset*np.pi/180, -self.offset*np.pi/180 + 2*np.pi])
		self.axrec.set_ylim(self.axrec.get_ylim())

	def onscroll(self, event):
		if not event.inaxes: return
		if event.inaxes is self.axpol:
			step = 0.25
			rmax = self.axpol.get_rmax()
			if event.button == 'up': rmax = max(step, rmax-step)
			else: rmax += step
			self.axpol.set_rlim([0, rmax])
			self.fig.canvas.draw()

	def onkeydown(self, event):
		if event.key == '?': self.print_help()
		elif self.mode.endswith('drag'): return
		elif event.key == 'm': 
			self.mode = 'move'
			self.fig.canvas.set_window_title('MOVE mode')
		elif event.key == 'v': 
			self.mode = 'visual'
			self.fig.canvas.set_window_title('VISUAL mode')
		elif event.key == 'escape': 
			self.mode = 'normal'
			self.fig.canvas.set_window_title('NORMAL mode')

		elif self.mode.startswith('move'):
			#if event.xdata is None: return

			#rmax = self.axpol.get_rmax()
			thetalim = np.array((self.axpol.get_thetamin(), self.axpol.get_thetamax()))
			xlim = list(self.axrec.get_xlim())
			#if event.key == 'up':
			#	thetalim[0] = min(thetalim[0]+10, 355)
			#	#thetalim[1] = max(thetalim[1]-10, 185)
			#	thetalim[1] = max(thetalim[1]-10, 5)
			#elif event.key == 'down':
			#	thetalim[0] = max(thetalim[0]-10, 5)
			#	#thetalim[0] = max(thetalim[0]-10, 0)
			#	thetalim[1] = min(thetalim[1]+10, 360)
			#elif event.key == 'right':
			#	thetalim[0] = max(thetalim[0]-10, 355)
			#	thetalim[1] = max(thetalim[1]-10, 360)
			#elif event.key == 'left':
			#	thetalim[0] = min(thetalim[0]+10, 355)
			#	thetalim[1] = min(thetalim[1]+10, 360)
			step = 3
			if event.key == 'up':
				thetalim[0] = thetalim[0]+step
				thetalim[1] = thetalim[1]-step
				xlim[0] += step*np.pi/180
				xlim[1] -= step*np.pi/180
			if event.key == 'down':
				thetalim[0] = thetalim[0]-step
				thetalim[1] = thetalim[1]+step
				xlim[0] -= step*np.pi/180
				xlim[1] += step*np.pi/180
			if event.key == 'right':
				thetalim[0] = thetalim[0]-step
				thetalim[1] = thetalim[1]-step
				xlim[0] -= step*np.pi/180
				xlim[1] -= step*np.pi/180
			if event.key == 'left':
				thetalim[0] = thetalim[0]+step
				thetalim[1] = thetalim[1]+step
				xlim[0] += step*np.pi/180
				xlim[1] += step*np.pi/180
			self.axpol.set_thetamin(thetalim[0])
			self.axpol.set_thetamax(thetalim[1])
			self.axrec.set_xlim(xlim)

			#print(rmax)
			#rmax = max(rmax - displacement[1], 1)
			#print(rmax)

			#self.axpol.set_rlim([0, rmax])
			self.fig.canvas.draw()


	def print_help(self):
		print('?: Show this help page')
		print('ESC: Return to normal mode')
		print('m: Enter move mode')
		print('v: Enter visual selection mode')

	def onpick(self, event):
		if self.mode.startswith('visual'): return
		print(event)
		print(event.artist)
		print(event.artist.hsp)

	def onmousedown(self, event):
		if self.mode == 'visual':
			self.mode = 'visualdrag'
			pos = np.array((event.xdata, event.ydata))
			self.transientents['visualwedge'] = self.axpol.bar(event.xdata, self.axpol.get_ylim()[1], 0.01, bottom=0, fc=(0.0, 0.5, 1.0, 0.5), zorder=1.2)
			self.transientents['visualwedge2'] = self.axrec.bar(event.xdata, self.axpol.get_ylim()[1], 0.01, bottom=0, fc=(0.0, 0.5, 1.0, 0.5), zorder=1.2)
			self.transientents['visualwedge'].startpos = pos
			self.transientdisp['visualwedge'] = np.zeros(2)
			self.transientdisp['lastpos'] = pos
			self.fig.canvas.draw()
		#elif self.mode == 'move':
		#	self.mode = 'movedrag'
		#	pos = np.array((event.xdata, event.ydata))
		#	self.transientdisp['movestart'] = pos
		#	self.fig.canvas.draw()
		#print(event.xdata, event.ydata)
	def onmouseup(self, event):
		#print(event.xdata, event.ydata)
		if self.mode == 'visualdrag':
			self.mode = 'visual'
			rect = self.transientents['visualwedge'].patches[0]
			self.update_rectplot(rect)

			self.transientents['visualwedge'].remove()
			self.transientents['visualwedge2'].remove()
			self.transientents.pop('visualwedge')
			self.transientents.pop('visualwedge2')
			self.transientdisp.pop('visualwedge')
			self.transientdisp.pop('lastpos')
			self.fig.canvas.draw()
		#elif self.mode == 'movedrag':
		#	self.mode = 'move'
		#	self.transientdisp.pop('movestart')
	def _handle_wraparound(self, pos1, pos2, threshold=np.pi/2):
		displacement = pos2 - pos1
		#case 1: >0 to <2pi: disp -= 2pi
		if (0 <= pos1[0] <= threshold) and (2*np.pi-threshold <= pos2[0] <= 2*np.pi): displacement[0] -= 2*np.pi
		#case 2: <2pi to >0: disp += 2pi
		elif (0 <= pos2[0] <= threshold) and (2*np.pi-threshold <= pos1[0] <= 2*np.pi): displacement[0] += 2*np.pi
		#case 3: beyond threshold
		else: pass
		return displacement

	def onmousemove(self, event):
		if not self.mode.endswith('drag'): return
		if self.mode == 'visualdrag':
			if event.xdata is None: return
			ent = self.transientents['visualwedge']
			ent2 = self.transientents['visualwedge2']
			#displacement = [p[0]-p[1] for p in zip((event.xdata, event.ydata), ent.startpos)]
			pos = np.array((event.xdata, event.ydata))
			#displacement = pos - self.transientdisp['lastpos']
			displacement = self._handle_wraparound(self.transientdisp['lastpos'], pos)
			self.transientdisp['lastpos'] = pos
			self.transientdisp['visualwedge'] += displacement
			for rect in ent.patches: 
				rect.set_width(self.transientdisp['visualwedge'][0])
			for rect in ent2.patches: 
				rect.set_width(self.transientdisp['visualwedge'][0])
			#ent.width = displacement[0]
			self.fig.canvas.draw()
		#print(event.xdata, event.ydata)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('--tblastn', action='store_true', help='Use tblastn. --query should point to a protein FASTA instead of a nucleotide FASTA')
	parser.add_argument('-e', '--evalue', type=float, default=1e-20, help='E-value threshold (default: 1e-20)')
	parser.add_argument('-i', '--ident', type=float, default=0.0, help='Identity cutoff given as a float between 0.0 and 1.0 (default: 0.0)')
	parser.add_argument('-l', '--length', type=float, default=None, help='Minimum length (default: None)')

	parser.add_argument('-q', action='store_true', help='Quiet mode')
	parser.add_argument('-o', help='Save figure here')
	parser.add_argument('--outfile-seq', help='Where to write sequence data to (default: qfrag_(HASH).fnt')
	parser.add_argument('--dpi', type=int, default=300, help='DPI for figure (if saved)')
	parser.add_argument('--width', type=float, default=6, help='Width of figure (default: 6)')
	parser.add_argument('--height', type=float, default=9, help='Height of figure (default: 9)')

	parser.add_argument('--query', required=True, nargs='+', help='Query filename(s)')
	parser.add_argument('--subject', required=True, nargs='+', help='Subject filename(s)')

	args = parser.parse_args()

	blastresults = {}
	circplot = Circplot(width=args.width, height=args.height, outfile_seq=args.outfile_seq)

	querycmap = matplotlib.cm.get_cmap('Accent')
	querycols = {}
	querydiv = 4
	query_index = 0

	for qfn in args.query:
		with open(qfn) as f:
			for qrec in SeqIO.parse(f, 'fasta'):
				#querycols[qrec.description] = querycmap((query_index%querydiv)/querydiv)
				#querycols[qrec.description + '_inv'] = querycmap(((query_index%querydiv)/querydiv + 1/querydiv/2) % 1)
				query = Genome(name=qrec.name, description=qrec.description, length=len(qrec))
				querycols[qrec.description] = 'k'
				querycols[qrec.description + '_inv'] = 'k'
				query_index += 1
				circplot.add_query(query)
		for sfn in args.subject:
			#if sfn not in blastresults: blastresults[sfn] = {}
			#blastresults[sfn][qfn] = blast(qfn, sfn, evalue=args.evalue)
			#blastresults.update(blast(qfn, sfn, evalue=args.evalue, length=args.length))
			hsps = blast(qfn, sfn, evalue=args.evalue, ident=args.ident, length=args.length, tblastn=args.tblastn)
			with open(sfn) as f:
				for srec in SeqIO.parse(f, 'fasta'):
					genome = Genome(name=srec.name, description=srec.description, length=len(srec))
					if srec.description in hsps:
						for queryhit in hsps[srec.description]:
							for hsp in hsps[srec.description][queryhit]:
								if hsp.qspan[1] >= hsp.qspan[0]: hsp.color = querycols[queryhit]
								else: hsp.color = querycols[queryhit + '_inv']
								hsp.genome = genome
								circplot.add_hsp(hsp)
					circplot.add_genome(genome)

	circplot.run()
