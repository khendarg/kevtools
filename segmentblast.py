#!/usr/bin/env python

import subprocess
from Bio.Blast import NCBIXML
from Bio import SeqIO
import numpy as np

import argparse
import sys
import io

HYDRO = {'G':-0.400,
	'I':4.500,
	'S':-0.800,
	'Q':-3.500,
	'E':-3.500,
	'A':1.800,
	'M':1.900,
	'T':-0.700,
	'Y':-1.300,
	'H':-3.200,
	'V':4.200,
	'F':2.800,
	'C':2.500,
	'W':-0.900,
	'K':-3.900,
	'L':3.800,
	'P':-1.600,
	'N':-3.500,
	'D':-3.500,
	'R':-4.500,
	'B':-3.500,
	'J':-3.500,
	'Z':4.150
}

def count_something(query, callback, normalize=False, weight=False):
	if normalize: dtype = np.float64
	else: dtype = np.int64

	arr = np.zeros(query.query_length, dtype=dtype)
	n = 0
	for alignment in query.alignments:
		n += 1 if not weight else 0

		for hsp in alignment.hsps:
			n += hsp.bits if weight else 0

			q = hsp.query_start
			for i in range(len(hsp.query)):
				if callback(hsp, i): arr[q-1] += hsp.bits if weight else 1
				if hsp.query[i] != '-': q += 1

	if normalize: arr /= n
	return arr

def _is_identity(hsp, index): return True if hsp.query[index] == hsp.sbjct[index] else False
def count_identities(query, normalize=False, weight=False):
	return count_something(query, _is_identity, normalize=normalize, weight=weight)

def _is_similarity(hsp, index): return True if hsp.match[index] != ' ' else False
def count_similarities(query, normalize=False, weight=False):
	return count_something(query, _is_similarity, normalize=normalize, weight=weight)

def _is_insertion(hsp, index): return True if hsp.query[index] == '-' else False
def count_insertions(query, normalize=False, weight=False):
	return count_something(query, _is_insertion, normalize=normalize, weight=weight)

def _is_deletion(hsp, index): return True if hsp.sbjct[index] == '-' else False
def count_deletions(query, callback=_is_deletion, normalize=False, weight=False):
	arr = count_something(query, _is_deletion, normalize=normalize, weight=weight)

	if normalize: dtype = np.float64
	else: dtype = np.int64

	arr = np.zeros(query.query_length, dtype=dtype)
	n = 0
	for alignment in query.alignments:
		n += 1 if not weight else 0
		for hsp in alignment.hsps:
			n += hsp.bits if weight else 0
			for i in range(hsp.query_start): 
				arr[i] += hsp.bits if weight else 1
			q = hsp.query_start
			for i in range(len(hsp.query)):
				if callback(hsp, i): arr[q-1] += hsp.bits if weight else 1
				if hsp.query[i] != '-': q += 1
			for i in range(hsp.query_end, query.query_length): 
				arr[i] += hsp.bits if weight else 1
	if normalize: arr /= n
	return arr

def average_hydropathies(query, weight=False):
	dtype = np.float64
	arr = np.zeros(query.query_length, dtype=dtype)
	n = np.ones(query.query_length, dtype=dtype)

	for alignment in query.alignments:
		for hsp in alignment.hsps:
			q = hsp.query_start
			for i in range(len(hsp.query)):
				if hsp.query[i] != '-': 
					n[q-1] += hsp.bits if weight else 1
					arr[q-1] += HYDRO[hsp.query[i]] * (hsp.bits if weight else 1)
					q += 1
	arr /= n
	return arr

def plot_stuff(obj, ax=None, window=None):
	interactive = False

	if ax is None:
		interactive = True
		import matplotlib.pyplot as plt
		fig, ax = plt.subplots()
	maxlen = 0
	tot = None
	for query in obj:
		#print(dir(query))
		for prop in obj[query]:
			arr = obj[query][prop]
			maxlen = max(maxlen, len(arr))

			X = np.arange(len(arr))

			#if prop == 'hydropathies': arr = np.convolve(arr, np.ones(window)/window, 'valid')
			if window: 
				arr = np.convolve(arr, np.ones(window)/window, 'valid')
				X = np.arange(len(arr))
				X += window//2
			if tot is None: tot = np.zeros(len(arr))
			tot += arr

			ax.plot(X, arr, label='{}/{}'.format(query.query_id, prop))
	ax.plot(X, tot, label='{}/total'.format(query.query_id))
	ax.axhline(0, color='k', lw=0.5)
	ax.axvline(0, color='k', lw=0.5)
	ax.axvline(maxlen, color='k', lw=0.5)
	ax.legend()

	if interactive: plt.show()
					

def print_usage():
	out = '''usage: segmentblast.py [-h] [--normalize] [--weight] BLAST ARGS
optional arguments:
  -h, --help   show this help message and exit
  --normalize  normalize values by the number of hits. Interacts with --weight
  --weight     weight values by bitscore. Interacts with --normalize
  BLAST ARGS   BLAST arguments like -db, -evalue, and -query. -outfmt is reserved for internal use'''
	print(out)

def main(blastargs):
	cmd = ['blastp']
	mode = 'plaintext'

	normalize = False
	if '--normalize' in blastargs: 
		normalize = True
		blastargs.remove('--normalize')

	weight = False
	if '--weight' in blastargs: 
		weight = True
		blastargs.remove('--weight')

	plot = False
	if '--plot' in blastargs:
		plot = True
		blastargs.remove('--plot')

	window = 19
	if '--window' in blastargs:
		window = int(blastargs[blastargs.find('--window') + 1])
		blastargs.pop(blastargs.index('--window')+1)
		blastargs.pop(blastargs.index('--window'))

	if '-h' in blastargs or '--help' in blastargs:
		print_usage()
		return 0

	for arg in blastargs: 
		if arg == '--plaintext': mode = 'plaintext'
		else: cmd.append(arg)


	cmd.extend(['-outfmt', '5'])

	if '-query' in cmd:
		try: qseqlist = list(SeqIO.parse(cmd[cmd.index('-query')+1], 'fasta'))
		except IndexError: raise IndexError('No argument found for -query')

		try: out = subprocess.check_output(cmd)
		except subprocess.CalledProcessError as e:
			print(e)
			return 1

	else:
		with open('/dev/stdin') as fh:
			rawqseqlist = fh.read().encode('utf-8')
			tf = io.StringIO(rawqseqlist.decode('utf-8'))
			tf.seek(0)

			qseqlist = list(SeqIO.parse(tf, 'fasta'))

			try: 
				p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
				out, err = p.communicate(input=rawqseqlist)
			except subprocess.CalledProcessError as e:
				print(e)
				return 1


	fout = io.BytesIO()
	try: fout.write(out)
	except ZeroDivisionError: fout.write(out.encode('iso-8859-1'))
	fout.flush()
	fout.seek(0)

	results = NCBIXML.parse(fout)

	obj = {}
	#for record in qseqlist: print(record)
	for query, qseq in zip(results, qseqlist):
		idents = count_identities(query, normalize=normalize, weight=weight)
		simils = count_similarities(query, normalize=normalize, weight=weight)
		insertions = count_insertions(query, normalize=normalize, weight=weight)
		deletions = count_deletions(query, normalize=normalize, weight=weight)
		hydropathies = average_hydropathies(query, weight=weight)
		print('#position\tidentities\tsimilarities\tinsertions\tdeletions\thydropathy')
		for i in range(query.query_length):
			print('{index}\t{ident}\t{simil}\t{insert}\t{delete}\t{hydro}'.format(index=i, ident=idents[i], simil=simils[i], insert=insertions[i], delete=deletions[i], hydro=hydropathies[i]))

		obj[query] = {
			'identities':idents,
			'similarities':simils,
			'insertions':insertions,
			'deletions':deletions,
			'hydropathies':hydropathies,
		}

	if plot: plot_stuff(obj, window=window)
	return obj

if __name__ == '__main__': main(sys.argv[1:])
