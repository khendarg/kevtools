#!/usr/bin/env python
from __future__ import print_function
from Bio import SeqIO
from Bio.Alphabet import IUPAC
import argparse
import sys
import subprocess

#    1   2     3   4
k = 'ACTGKMRSWYBDHVNX'
v = 'TGACMKYSWRVHDBNX'

COMPLEMENT = dict(zip(k, v))
AMBIGUITY = k[4:]

def error(*things): 
	print('[ERROR]', *things, file=sys.stderr)
	exit(1)
def info(*things): print('[INFO]', *things, file=sys.stderr)
def warn(*things): print('[WARN]', *things, file=sys.stderr)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infile', nargs='*', default=['/dev/stdin'], help='Nucleotide FASTA(s) to read in. (default:stdin)')
	parser.add_argument('-o', default='/dev/stdout', help='Where to write the resulting sequences (default:stdout)')

	args = parser.parse_args()

	if args.o.endswith('stdout'): outfile = '/dev/stdout'
	else: outfile = args.o

	with open(args.o, 'w') as fout:
		for fn in args.infile:
			if fn.endswith('stdin'): fn = '/dev/stdin'
			with open(fn) as fin:
				writeme = []
				for record in SeqIO.parse(fin, 'fasta', alphabet=IUPAC.ambiguous_dna):
					writeme.append(record.reverse_complement())
					writeme[-1].name = record.name + '.REVCOMP'
					writeme[-1].id= record.id + '.REVCOMP'
					writeme[-1].description = record.description + ' REVCOMP'
				SeqIO.write(writeme, fout, 'fasta')

