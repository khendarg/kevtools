#!/usr/bin/env python

from __future__ import print_function, division, unicode_literals

import os
import argparse
import subprocess
import shlex
import io
import sys
import tempfile
import re
import numpy as np

import scipy.stats
import quicklsat

from Bio import SeqIO
import shuffle_sequences_within_class as shuffleseq

def error(*things):
    print('[ERROR]', *things, file=sys.stderr)
    exit(1)

NUMBINS = 20

def main(queryfn, subjectfn, count=1000, mode='swzscore', outdir=None, flags=None, split_inout=False, split_looptail=False, shufbuffer=1000, interactive=False, expectsmooth=5, quiet=False, binsize=None, bandwidth=None, preview=False):
    flags = '' if flags is None else flags

    args = []
    ignore = True
    for flag in shlex.split(flags):
        if flag.startswith('-'):
            ignore = False
            if flag.startswith('-E'): ignore = True
            elif flag.startswith('-m'): ignore = True
            elif flag.startswith('-k'): ignore = True
            if not ignore: args.append(flag)
        elif ignore: continue
        else:
            args.append(flag)
    args.extend(['-E', str(count), '-k', '1000', '-m', '10'])

    if outdir: 
        if not os.path.isdir(outdir): os.mkdir(outdir)
        qfh = open('{}/query.faa'.format(outdir), 'w')
        rsfh = open('{}/subject.faa'.format(outdir), 'w+')
        ssfh = open('{}/subject_shuffled.faa'.format(outdir), 'w')
    else:
        qfh = tempfile.NamedTemporaryFile()
        rsfh = tempfile.NamedTemporaryFile()
        ssfh = tempfile.NamedTemporaryFile()

    if queryfn.startswith('asis:'): qfh.write('>query\n{}'.format(queryfn[5:]))
    else: 
        with open(queryfn) as f: qfh.write(f.read())
    if subjectfn.startswith('asis:'): rsfh.write('>subject\n{}'.format(subjectfn[5:]))
    else:
        with open(subjectfn) as f: rsfh.write(f.read())

    qfh.flush()
    rsfh.flush()
    rsfh.seek(0)



    #not sure if this should be scaled up
    sseq = SeqIO.read(rsfh, 'fasta')
    rsfh.seek(0)

    mergedict = shuffleseq.get_mergedict(split_inout, split_looptail)

    remaining = count
    ssfh.write(rsfh.read() + '\n')
    for i in range(0, count, shufbuffer):
        if remaining < shufbuffer: numshuf = remaining % shufbuffer
        else: numshuf = shufbuffer

        sequences = shuffleseq.shuffle_seq(sseq, count=numshuf, prefix='shuf', mergedict=mergedict)
        ssfh.write('\n'.join(sequences))
        remaining -= numshuf
    ssfh.flush()

    ssfh.seek(0)
    alnfh = tempfile.NamedTemporaryFile("w")
    alnfh.seek(0)

    for record in SeqIO.parse(alnfh.name, 'fasta'):
        print(record)

    ssresults = ''
    with open(ssfh.name) as fh: 
        for record in SeqIO.parse(fh, 'fasta'):
            SeqIO.write([record], alnfh, 'fasta')
            alnfh.flush()

            out = subprocess.check_output(['ssearch36'] + args + [qfh.name, alnfh.name])
            out = out.decode('utf-8')
            ssresults += out + '\n'

            alnfh.seek(0)
            alnfh.truncate()
    stats = get_statistic(ssresults, accession=sseq.name, mode=mode, expectsmooth=expectsmooth, binsize=binsize, bandwidth=bandwidth, preview=preview)
    fmtout = quicklsat.get_formatted(stats)
    if outdir:
        with open('{}/results.ssearch'.format(outdir), 'w') as f: f.write(ssresults)
        with open('{}/results.lsat'.format(outdir), 'w') as f: f.write(fmtout)
    if not quiet: print(fmtout)
    return fmtout
        
    if outdir:
        with open('{}/results.ssearch'.format(outdir), 'w') as f:
            pass


    out = subprocess.check_output(['ssearch36'] + args + [qfh.name, ssfh.name])
    out = out.decode('utf-8')

    if outdir: 
        with open('{}/results.ssearch'.format(outdir), 'w') as f: f.write(out)
    stats = quicklsat.get_statistic(out, accession=sseq.name, mode=mode, expectsmooth=expectsmooth, binsize=binsize, bandwidth=bandwidth)
    fmtout = quicklsat.get_formatted(stats)
    if outdir: 
        with open('{}/results.lsat'.format(outdir), 'w') as f: f.write(fmtout)
    #print(stats)


    if interactive: raw_input(qfh.name + ' ' + ssfh.name)
    if not quiet: print(fmtout)
    return (quicklsat.get_formatted(stats))


def get_statistic(text, accession, mode='swzscore', expectsmooth=5, binsize=None, bandwidth=None, preview=False):
    section = ''
    subsection = ''

    rank = 1
    hits = 0

    currentacc = ''
    runtime = {}
    scores = []
    bitscores = []
    special = {}
    statistics = {}
    sequence = {}

    bestexpect = []
    allexpect = []

    multiline = False
    mlkey = ''
    for l in text.split('\n'):
        if l.startswith('>>>'): section = 'runtime'
        elif not section: continue
        elif l.startswith('>>'): 
            section = 'alignment'
            currentacc = l[2:].strip()
            special[currentacc] = {}
            if currentacc == accession or (not currentacc.startswith('shuf_')): statistics['rank'] = rank
            rank += 1
            multiline = False
        elif l.startswith('>'): 
            section = 'sequence'
            if currentacc == accession or (not currentacc.startswith('shuf_')): 
                if '_order' not in sequence: sequence['_order'] = []
                mlkey = l[1:].strip()
                sequence['_order'].append(mlkey)
                multiline = True

        elif multiline and not l.startswith('; ') and (currentacc == accession):
            if mlkey in sequence: sequence[mlkey] += l.replace('\n', '')
            else: sequence[mlkey] = l.replace('\n', '')

        elif subsection: pass

        elif l.startswith(';'): 
            if section == 'runtime':
                k, v = quicklsat.get_kv(l)
                runtime[k] = v
            elif section == 'alignment':
                k, v = quicklsat.get_kv(l)
                if currentacc == accession or not currentacc.startswith('shuf_'): special[currentacc][k] = v
                elif ' opt' in k: scores.append(v)
                elif 'bits' in k: bitscores.append(v)

                if currentacc.startswith('shuf_') and 'expect' in k:
                    if (len(bestexpect) < expectsmooth) and v > 0: bestexpect.append(v)
                    allexpect.append(v)
            elif section == 'sequence':
                k, v = quicklsat.get_kv(l)
                if (currentacc == accession) or (not currentacc.startswith('shuf_')): 
                    if 'al_display_start' in k: sequence[mlkey] = ''
                    elif 'al_cons' in k: 
                        mlkey = 'cons'
                        sequence['_order'].append(mlkey)
                        sequence[mlkey] = ''
                    else: special[currentacc][k] = v
    #print(special)

    if accession not in special: 
        if special:
            #FIXME: Better handling for complex FASTA headers
            k = sorted(special)[0]
            special[accession] = special[k]
        else: raise Exception('Could not find subject sequence in {} alignments'.format(len(bitscores)))


    #import matplotlib.pyplot as plt
    ##plt.hist(bitscores)
    #X = np.arange(np.min(bitscores), np.max(bitscores), (np.max(bitscores) - np.min(bitscores)) * 0.01)
    #Y = kde.evaluate(X)
    #plt.plot(X, Y)
    #print('!!!!!!!' * 100)
    #plt.show()

    for k in special[accession]:
        if 'opt' in k: 
            statistics['swmean'] = np.mean(scores)
            statistics['swstd'] = np.std(scores)
            statistics['swzscore'] = (special[accession][k] - statistics['swmean']) / statistics['swstd']
            statistics['count'] = len(scores)
        elif 'bits' in k:
            statistics['bitsmean'] = np.mean(bitscores)
            statistics['bitsstd'] = np.std(bitscores)
            statistics['bitszscore'] = (special[accession][k] - statistics['bitsmean']) / statistics['bitsstd']

            allbitscores = np.concatenate([bitscores, [special[accession][k]]])
            kde = scipy.stats.gaussian_kde(allbitscores, bw_method=bandwidth)
            statistics['p-value'] = kde.integrate_box_1d(special[accession][k], 9e99)
            statistics['p-value-2x'] = kde.integrate_box_1d(special[accession][k], 2*np.max(allbitscores))

            histcounts, histbins = np.histogram(allbitscores, bins=NUMBINS) 
            curarea = np.sum(histcounts * (histbins[1] - histbins[0]))
            normcounts = histcounts / curarea

            histX = (histbins[:-1] + histbins[1:]) / 2
            histY = normcounts
            histYkde = kde.evaluate(histX)

            histkde_mae = np.mean(np.abs(histYkde - histY))
            histkde_mse = np.mean((histYkde - histY)**2)
            statistics['histkde_mae'] = histkde_mae
            statistics['histkde_mse'] = histkde_mse

            if preview:

                import matplotlib.pyplot as plt
                X = np.arange(np.min(allbitscores), np.max(allbitscores), (np.max(allbitscores) - np.min(allbitscores)) * 0.001)
                Y = kde.evaluate(X)
                plt.plot(X, Y, label='KDE')
                plt.plot(histX, histY, label='Histogram centers')
                plt.legend()
                plt.scatter([special[accession][k]], [kde.evaluate(special[accession][k])])
                plt.show()


        elif 'expect' in k:
            topexpect = 10 ** np.mean(np.log10(bestexpect))
            if special[accession][k] == 0: 
                statistics['expectratio'] = -1
                statistics['medianexpectratio'] = -1
                statistics['log10expectratio'] = -1
                statistics['medianexpect'] = -1
            else: 
                statistics['expectratio'] = topexpect / special[accession][k] 
                statistics['log10expectratio'] = np.log10(statistics['expectratio'])
                statistics['medianexpectratio'] = allexpect[len(allexpect)//2] / special[accession][k] 
                statistics['medianexpect'] = allexpect[len(allexpect)//2]
            statistics['numexpect'] = len(bestexpect)

            #if special[accession][k] > (0.4 * bestexpect[0]):
            #    print(statistics['expectratio'], special[accession][k], 10 ** np.mean(np.log10(bestexpect)), bestexpect, file=sys.stderr)

    statistics['accession'] = accession
    return {'runtime':runtime, 'alignment':special[accession], 'statistics':statistics, 'sequence':sequence}


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--flags', help='Flags to pass on to ssearch as a quoted string. -E, -m, and -k will be ignored.')
    parser.add_argument('-c', '--count', type=int, default=1000, help='Number of shuffles to perform (default:1000)')
    parser.add_argument('-e', type=int, default=10, help='Number of top shuffled alignments to compare the unshuffled alignment against (default:10)')
    parser.add_argument('-i', action='store_true', help='Prompt with filenames to allow manual inspection')
    parser.add_argument('-m', '--mode', default='swzscore', help='Statistic to use (\033[1mswzscore\033[0m, bitzscore, rank)')
    parser.add_argument('-o', '--outdir', help='Where to write intermediate files (default: nowhere)')

    parser.add_argument('-q', '--quiet', action='store_true', help='Don\'t print anything to stdout')

    parser.add_argument('-s', action='store_true', help='Split loop residues by in/out')
    parser.add_argument('-t', action='store_true', help='Split residues by loop/tail')

    parser.add_argument('--binsize', type=float, default=None, help='Bin size for histogram (default: auto)')
    parser.add_argument('--bandwidth', type=float, default=None, help='Bandwidth for (default: auto (Scott\'s method)')
    parser.add_argument('--show', action='store_true', help='Plot the KDE and the histogram')

    parser.add_argument('query')
    parser.add_argument('subject')

    args = parser.parse_args()

    if args.mode not in ('swzscore', 'bitzscore', 'rank'): error('Invalid mode "{}"'.format(args.mode))

    main(args.query, args.subject, count=args.count, mode=args.mode, outdir=args.outdir, flags=args.flags, split_inout=args.s, split_looptail=args.t, interactive=args.i, expectsmooth=args.e, quiet=args.quiet, binsize=args.binsize, bandwidth=args.bandwidth, preview=args.show)
