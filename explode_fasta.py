#!/usr/bin/env python

from Bio import SeqIO
import argparse
import os

def save(fastalist, outdir):
	if not os.path.isdir(outdir): os.mkdir(outdir)

	for record in fastalist:
		fn = '{}/{}.faa'.format(outdir, record.name.split()[0])
		with open(fn, 'w') as f:
			f.write('>{}\n{}\n'.format(record.name.split()[0], record.seq))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile')
	parser.add_argument('outdir')

	args = parser.parse_args()

	with open(args.infile) as f:
		fastalist = SeqIO.parse(f, 'fasta')
		save(fastalist, args.outdir)
