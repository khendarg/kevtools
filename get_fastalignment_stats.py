#!/usr/bin/env python
from __future__ import print_function, division

import argparse
import re
import Bio.AlignIO
import numpy as np

from kevtools_common.types import TCID
from kevtools_common import fastatools

def get_zscore(a, q):
	return (q - np.mean(a)) / np.std(a)

def get_normal(alignments, testaln, attrib='score'):
	if attrib == 'score':
		a = alignments.get_score()
		q = testaln.score
	elif attrib == 'bitscore':
		a = alignments.get_bitscore()
		q = testaln.bitscore
	elif attrib == 'evalue':
		a = alignments.get_evalue()
		q = testaln.evalue
	elif attrib == 'ident':
		a = alignments.get_ident()
		q = testaln.ident
	elif attrib == 'simil':
		a = alignments.get_simil()
		q = testaln.simil

	else: raise NotImplementedError('Not implemented: attrib={}'.format(attrib))

	mean = np.mean(a)
	std = np.std(a)
	z = get_zscore(a, q)
	return mean, std, z, q
	
def find_targetseq(alignments, name=None):
	testaln = None
	for aln in alignments:
		if name:
			if aln.target.name.startswith(name): testaln = aln
		else: 
			try: 
				TCID(aln.target.name)
				testaln = aln
			except AssertionError: pass
	return testaln

def gsat_style(a, q, attrib):
	out = ''
	mean, std, zscore, raw = get_normal(a, q, attrib)
	out += 'Average Quality (AQ)    {:0.2f} +/- {:0.2f}\n'.format(mean, std)
	out += 'Standard score (Z): {:0.1f}\n'.format(zscore)
	out += 'Precise score (Z):  {:0.3f}\n'.format(zscore)
	out += 'Raw score:  {}'.format(raw)
	return out

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('--target', default=None, help='Target sequence label. If not specified, returns stats for the first TCID-like sequence')
	parser.add_argument('infile')
	args = parser.parse_args()

	with open(args.infile) as f:
		alignments = fastatools.AlignmentCollection.parse(f)

		interestingaln = find_targetseq(alignments, args.target)
		if interestingaln is None: raise ValueError('Could not find a suitable alignment (searched {})'.format(len(alignments)))

		print(interestingaln.emboss_style())
		print('============ FINISHED =============')
		print('#S-W stats (~GSAT behavior):')
		print(gsat_style(alignments, interestingaln, 'score') + '\n')
		print('#Bitscore stats:')
		print(gsat_style(alignments, interestingaln, 'bitscore') + '\n')
		print('#Identity stats:')
		print(gsat_style(alignments, interestingaln, 'ident') + '\n')
		print('#Similarity stats:')
		print(gsat_style(alignments, interestingaln, 'simil') + '\n')

