#!/usr/bin/env python

import argparse
import re
import os
import fasta_grepper

def main(p1dir, p2dir, outdir, count=100):
	if not os.path.isdir(p2dir): raise IOError('p2dir "{}" does not exist'.format(p2dir))
	if not os.path.isfile('{}/report.tbl'.format(p2dir)): raise IOError('reports.tbl not found in p2dir "{}"'.format(p2dir))

	if not os.path.isdir(outdir): os.mkdir(outdir)

	subjectaccs = set()
	targetaccs = set()
	pairs = []
	with open('{}/report.tbl'.format(p2dir)) as f:
		for l in f:
			if l.startswith('#'): continue
			elif not l.strip(): continue
			elif 'Subject' in l: continue

			else:
				sl = l.split('\t')
				acc1 = sl[0]
				acc2 = sl[1]

				subjectaccs.add(acc1)
				targetaccs.add(acc2)
				pairs.append([acc1, acc2])

			if len(pairs) >= count: break

	fams = os.path.basename(p2dir).split('_vs_')

	if not os.path.isdir('{}/sequences'.format(outdir)): os.mkdir('{}/sequences'.format(outdir))
	#for acc in subjectaccs.union(targetaccs):
	#	if os.path.isfile('{}/sequences/{}.faa'.format(outdir, acc)): continue

	#	foundit = False
	#	for fam in fams:
	#		sequences = fasta_grepper.main(acc, '{}/{}/results.faa'.format(p1dir, fam), single=True)
	#		print(sequences)
	#		if sequences:
	#			foundit = True
	#			with open('{}/sequences/{}.faa'.format(outdir, acc), 'w') as g: 
	#				for record in sequences:
	#					g.write('>{}\n{}\n'.format(record.name.split()[0], record.seq))
	#	if not foundit: raise ValueError('Could not find "{}" in "{}/{}/resultssubjects.faa"'.format(acc, p2dir))
	#		

	#exit()

	fs = open('{}/subjects.faa'.format(p2dir))
	ft = open('{}/targets.faa'.format(p2dir))
	for acc in subjectaccs:
		
		#seq = fasta_grepper.main(acc, '{}/subjects.faa'.format(p2dir), single=True)
		seq = fasta_grepper.main(acc, fs, single=True)
		if seq:
			with open('{}/sequences/{}.faa'.format(outdir, acc), 'w') as g: 
				for record in seq:
					g.write('>{}\n{}\n'.format(record.name.split()[0], record.seq))
		fs.seek(0)
		if not seq:
			seq = fasta_grepper.main(acc, ft, single=True)
			if seq: 
				with open('{}/sequences/{}.faa'.format(outdir, acc), 'w') as g: 
					for record in seq:
						g.write('>{}\n{}\n'.format(record.name.split()[0], record.seq))
			else: raise ValueError('Could not find "{}" in "{}/subjects.faa"'.format(acc, p2dir))
			ft.seek(0)

	for acc in targetaccs:
		
		#seq = fasta_grepper.main(acc, '{}/subjects.faa'.format(p2dir), single=True)
		seq = fasta_grepper.main(acc, ft, single=True)
		if seq:
			with open('{}/sequences/{}.faa'.format(outdir, acc), 'w') as g: 
				for record in seq:
					g.write('>{}\n{}\n'.format(record.name.split()[0], record.seq))
		ft.seek(0)
		if not seq:
			seq = fasta_grepper.main(acc, fs, single=True)
			if seq: 
				with open('{}/sequences/{}.faa'.format(outdir, acc), 'w') as g: 
					for record in seq:
						g.write('>{}\n{}\n'.format(record.name.split()[0], record.seq))
			else: raise ValueError('Could not find "{}" in "{}/subjects.faa"'.format(acc, p2dir))
			fs.seek(0)


	with open('{}/{}.pairlist'.format(outdir, os.path.basename(p2dir)), 'w') as f:
		for acc1, acc2 in pairs: f.write('{}\t{}\n'.format(acc1, acc2))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('p1dir')
	parser.add_argument('p2dir')
	parser.add_argument('outdir')
	parser.add_argument('-n', type=int, default=100, help='Maximum top protocol2 hits to rip')

	args = parser.parse_args()

	main(args.p1dir, args.p2dir, args.outdir, count=args.n)
