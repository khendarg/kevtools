#!/usr/bin/env python

import argparse
import sys
import os
import xml.etree.ElementTree as ET

def get_sequences(fh, include_flanks=False):
	doc = ET.parse(fh)
	root = doc.getroot()

	sequences = {}
	id2name = {}
	motiflengths = {}
	for section in root:
		if section.tag.endswith('sequences'):
			for sequence in section:
				name = sequence.attrib['name']
				for hsp in sequence:
					if hsp.tag.endswith('seg'):
						seqstart = int(hsp.attrib['start'])
						hspseq = ''
						for hspdata in hsp:
							if hspdata.tag.endswith('data'):
								hspseq = hspdata.text
							elif hspdata.tag.endswith('hit'):
								hsppos = int(hspdata.attrib['pos'])
								motifid = hspdata.attrib['idx']

						hspseq = hspseq.strip().replace('\n', '')
						if not include_flanks:
							hspseq = hspseq[hsppos - seqstart:hsppos-seqstart+motiflengths[motifid]]
						sequences[id2name[motifid]].append('>{}\n{}'.format(name, hspseq))
		elif section.tag.endswith('motifs'):
			for motif in section: 
				motifid = str(len(sequences))
				id2name[motifid] = motif.attrib['alt']
				motiflengths[motifid] = int(motif.attrib['length'])
				sequences[id2name[motifid]] = []
	return sequences
							
						

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infile')
	parser.add_argument('--include-flanks', action='store_true', help='Include flanking residues as recorded in MAST files')
	parser.add_argument('-o', '--outdir', help='Where to save sequences (default: write all to stdout)')

	args = parser.parse_args()

	if 'mast.xml' in args.infile: infmt = 'mast'
	else: print('[WARNING]: This does not look like MAST XML output', file=sys.stderr)

	with open(args.infile) as fh:
		sequences = get_sequences(fh, include_flanks=args.include_flanks)

		if args.outdir:
			if not os.path.isdir(args.outdir): os.mkdir(args.outdir)
			for motif in sequences:
				with open('{}/{}.faa'.format(args.outdir, motif), 'w') as outfh:
					for seq in sequences[motif]:
						outfh.write(seq + '\n')
		else:
			for motif in sequences:
				for seq in sequences[motif]:
					print(seq)
