#!/usr/bin/env python
#A small wrapper for requesting profTMB results remotely
from __future__ import print_function

import argparse
import os
try: 
	from urllib.request import urlopen
	from urllib.parse import urlencode
except ImportError: 
	from urllib import urlopen, urlencode


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='?', default='/dev/stdin', help='Where to read sequences from (default: stdin)')

	parser.add_argument('--outfmt', default='hmmtop', help='Which format to get TMSs in (default: hmmtop)')
	parser.add_argument('--evalue', default=1e-5, type=float, help='E-value threshold (default: 1e-5)')

	args = parser.parse_args()

	if 'PROFTMB_ENDPOINT' not in os.environ:
		print('Please set $PROFTMB_ENDPOINT to a URL pointing to proftmb.py')

	with open(args.infile) as f: seq = f.read()
	postdata = {'q': seq, 'outfmt': args.outfmt, 'evalue': args.evalue}

	try: data = urlencode(postdata).encode('utf-8')
	except AttributeError: data = urlencode(postdata)
	f = urlopen(os.environ['PROFTMB_ENDPOINT'], data=data)
	out = f.read()
	try: out = out.decode('utf-8')
	except AttributeError: pass
	print(out)
