#!/usr/bin/env python

import numpy as np
import argparse

def parse_columnspec(colstr):
	columns = set()
	for span in colstr.split(','):
		if '-' in span: 
			a, b = span.split('-')
			columns.update(range(int(a)-1, int(b)))
		else: columns.add(int(span)-1)

	return columns

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('-b', '--base', type=float, default=None, help='Logarithm base')
	parser.add_argument('-k', help='Columns to take logarithms for')
	parser.add_argument('infile', nargs='?', default='/dev/stdin', help='File to read in')

	args = parser.parse_args()

	if args.k: columns = parse_columnspec(args.k)
	else: columns = None


	out = ''
	with open(args.infile) as f:
		for l in f:
			sl = l.split('\t')

			if l.startswith('#'): 
				out += l
			elif columns:
				truesl = []
				for i, colpos in enumerate(sl):
					if i in columns:
						if args.base == 2: truesl.append(np.log2(float(colpos)))
						elif args.base == 10: truesl.append(np.log10(float(colpos)))
						elif args.base: truesl.append(np.log(float(colpos))/np.log(args.base))
						else: truesl.append(np.log(float(colpos)))
					else: truesl.append(float(colpos))
				out += '\t'.join([str(x) for x in truesl]) + '\n'
			else:
				if args.base == 2: out += '\t'.join([str(np.log2(float(x))) for x in sl]) + '\n'
				elif args.base == 10: out += '\t'.join([str(np.log10(float(x))) for x in sl]) + '\n'
				elif args.base: out += '\t'.join([str(np.log(float(x))/np.log(float(args.base))) for x in sl]) + '\n'
				else: out += '\t'.join([str(np.log(float(x))) for x in sl]) + '\n'
	print(out)
