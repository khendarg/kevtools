#!/usr/bin/env python

import argparse
import sys
import os
import xml.etree.ElementTree as ET
import numpy as np

def get_motifs(fh):
	doc = ET.parse(fh)
	root = doc.getroot()

	motifs = {}
	id2name = {}
	for section in root:
		if section.tag.endswith('motifs'):
			for motif in section: 
				motifid = motif.attrib['id']
				motifs[motifid] = []
				for subsection in motif: 
					if subsection.tag.endswith('scores'):
						for matrix in subsection:
							motifs[motifid] = None
							for i, arr in enumerate(matrix):
								if motifs[motifid] is None: motifs[motifid] = np.int64(np.zeros((len(matrix),20)))
								for j, position in enumerate(arr):
									if position.tag.endswith('value'):
										motifs[motifid][i,j] = int(position.text)
							
	return motifs
def score_aln(p1, p2, threshold=0):
	corrs = np.array([np.corrcoef(p1[i,:], p2[i,:])[1,0] for i in range(len(p1))])

	#we don't want to keep anticorrelations
	if (corrs < 0).all(): return 0, 0, [], 0
	else:
		start = None
		end = None
		for i, x in enumerate(corrs):
			if (start is None) and (x > threshold): start = i
			elif (start is not None) and (x > threshold): end = i

		if end is None or start is None: return 0, 0, [], 0
		length = end - start + 1
		score = sum(corrs[start:end+1])
		
		return sum(corrs), length, corrs, start

def align_motifs(motif1, motif2, threshold=0):
	offset = 0
	longer = motif1 if len(motif1) > len(motif2) else motif2
	shorter = motif1 if len(motif1) < len(motif2) else motif2
	if True:#len(motif1) != len(motif2):
		best = None
		for offset in range(len(longer) - len(shorter) + 1):
			lfrag = longer[offset:offset+len(shorter)]
			score, length, corrs, start = score_aln(lfrag, shorter, threshold)
			#print(score/length)

			if best is None: best = (offset, score, length, corrs, start)
			else: 
				if score > best[1]: 
					best = (offset, score, length, corrs, start)

		#print('>', best[1]/best[2])
		offset = best[0]

		return best
	#print(offset, score_aln(longer[offset:offset+len(shorter)], shorter))

def approx_logo(motif):
	seq = ''
	for pos in motif:
		r = '.'
		maxval = np.max(pos)
		
		for i, resn in enumerate('ACDEFGHIKLMNPQRSTVWY'):
			if pos[i] == maxval: 
				if np.max(pos) > (np.mean(pos) + 2 * np.std(pos)): r = resn
				elif np.max(pos) > (np.mean(pos) + 1 * np.std(pos)): r = resn.lower()
				break
		seq += r
	return seq

def pretty_midline(corrs):
	out = ''
	for corr in corrs:
		if corr >= 0.9: out += '|'
		elif corr >= 0.7: out += ':'
		elif corr >= 0.5: out += '.'
		else: out += ' '
	return out

def pretty_corrs(corrs):
	out = ''
	for r in corrs:
		if r < 0: out += '-'
		elif r >= 0.95: out += '9'
		else: out += '{:0.1f}'.format(r)[-1]
	return out
			
			
def pretty_print(motif1, motif2, offset, score, length, corrs, start):
	out = ''
	out += '# Score: {:0.1f}\n'.format(score)
	if length: 
		#out += '# Average correlation: {:0.1%}\n'.format(np.nanmean(corrs[offset:offset+length]))
		#out += '# Average squared correlation: {:0.1%}\n'.format(np.mean(np.power(corrs[offset:offset+length], 2)))
		out += '# Average correlation: {:0.1%}\n'.format(np.nanmean(corrs))
		out += '# Average squared correlation: {:0.1%}\n'.format(np.mean(np.power(corrs, 2)))
	else: 
		out += '# Average correlation: 0.0%\n'
		out += '# Average squared correlation: 0.0%\n'
	out += '# Length: {}\n'.format(length)
	out += '# Start: {}\n'.format(offset+start+1)
	out += '# End: {}\n'.format(offset+start+length)

	longer = motif1 if len(motif1) > len(motif2) else motif2
	shorter = motif1 if len(motif1) < len(motif2) else motif2

	aln = ''
	aln += 'alignd ' + (' '*(offset+start)) + ('+'*length) + '\n'
	aln += 'mobile ' + ('-'*offset) + approx_logo(shorter) + ('-'*(len(longer) - len(shorter) - offset)) + '\n'
	aln += 'midlin ' + (' '*offset) + pretty_midline(corrs) + '\n'
	#aln += 'alignr ' + (' '*offset) + ('+' * length) + (' '*(len(longer) - len(shorter) - offset)) + '\n'
	aln += 'target ' + approx_logo(longer) + '\n'
	aln += 'correl ' + (' '*offset) + (pretty_corrs(corrs)) + (' '*(len(longer) - len(shorter) - offset)) + '\n'

	out += aln + '\n'
	return out
				

if __name__ == '__main__':
	parser = argparse.ArgumentParser('MEME-to-MEME gapless alignment tool')
	parser.add_argument('infile', nargs='+')

	parser.add_argument('-l', '--laziness', type=float, default=0, help='Minimum correlation score at termini')
	parser.add_argument('-s', '--self-align', action='store_true', help='Perform within-file alignments. Ignored if only one file is given')

	args = parser.parse_args()

	for fn1 in args.infile:
		if 'meme.xml' in fn1: infmt = 'meme'
		else: print('[WARNING]: This does not look like MEME XML output', file=sys.stderr)

		fh1 = open(fn1)
		motifs1 = get_motifs(fh1)

		for fn2 in args.infile:

			if fn1 > fn2: continue
			if (fn1 == fn2) and (len(args.infile) > 1) and not args.self_align: continue

			fh2 = open(fn2)
			motifs2 = get_motifs(fh2)

			for m1 in motifs1:
				for m2 in motifs2:
					offset, score, length, corrs, start = align_motifs(motifs1[m1], motifs2[m2], args.laziness)
					print('# Mobile: {}: {}'.format(fn1, m1))
					print('# Target: {}: {}'.format(fn2, m2))
					print('# Laziness: {:0.1f}'.format(args.laziness))
					longer = motifs1[m1] if len(motifs1[m1]) > len(motifs2[m2]) else motifs2[m2]
					shorter = motifs1[m1] if len(motifs1[m1]) < len(motifs2[m2]) else motifs2[m2]
					print(pretty_print(motifs1[m1], motifs2[m2], offset, score, length, corrs, start))
