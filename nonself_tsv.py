#!/usr/bin/env python

import argparse

def get_nonself(fh, keys=(1,2), invert=False):
	lines = []
	for l in fh:
		if not l.strip(): continue
		elif l.startswith('#'): lines.append(l.replace('\n', ''))

		else:
			sl = l.split('\t')
			if (sl[keys[0]] != sl[keys[1]]) ^ invert:
				lines.append(l.replace('\n', ''))
	return lines

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='*', default=['/dev/stdin'], help='TSVs')
	parser.add_argument('-k', nargs=2, type=int, default=[1,2], help='1-indexed columns that must not match')
	parser.add_argument('-v', action='store_true', help='Invert results, i.e. return matched lines')

	args = parser.parse_args()

	keys = [k-1 for k in args.k]

	for fn in args.infile:
		with open(fn) as fh:
			lines = get_nonself(fh, keys=keys, invert=args.v)
			print('\n'.join(lines))
