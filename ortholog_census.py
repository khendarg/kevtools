#!/usr/bin/env python

import argparse
from kevtools_common.tcid import TCID
import os

#NCBI acc, UNP acc, TCID


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infiles', nargs='+')
	parser.add_argument('-y', default='Y', help='What to mark positives with')
	parser.add_argument('-n', default='', help='What to mark negatives with')
	args = parser.parse_args()

	proteomes = {}
	bns = []
	tcids = []
	for fn in args.infiles:
		bn = os.path.basename(fn)
		proteomes[bn] = []
		bns.append(bn)
		with open(fn) as f:
			for l in f:
				if not l.strip(): continue
				elif l.startswith('#'): continue

				sl = l.split('\t')

				ncbi = sl[0]
				unp = sl[1]
				tcid = TCID(sl[2])
				stuff = {'ncbi':ncbi, 'unp':unp, 'tcid':tcid}
				proteomes[bn].append(stuff)
				tcids.append((tcid, bn, stuff))

	umbrella_tcids = {}
	for tcid, bn, stuff in tcids:
		if tcid not in umbrella_tcids: umbrella_tcids[tcid] = []
		umbrella_tcids[tcid].append(bn)
	
	line = '#NCBI acc.'
	for p in bns: line += '\t{}'.format(p)
	print(line)
	for tcid in sorted(umbrella_tcids):
		line = ''
		for p in bns:
			if p in umbrella_tcids[tcid]: line += '\t{}'.format(args.y)
			else: line += '\t{}'.format(args.n)
		line += '\t' + str(tcid)
		print(line)
