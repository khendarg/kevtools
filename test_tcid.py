#!/usr/bin/env python 
import unittest
import kevtools_common

class TestTcid(unittest.TestCase):

	parent = kevtools_common.types.TCID('1.A')
	child = kevtools_common.types.TCID('1.A.1')
	other = kevtools_common.types.TCID('1.B')
	another = kevtools_common.types.TCID('1.B.1.1')

	longone = kevtools_common.types.TCID('1.A.1.1.1-ACC123')


	def test_contains(self):
		assert self.parent in self.parent
		assert self.child in self.parent
		assert self.other not in self.parent
		assert self.another not in self.parent

		assert self.parent not in self.child
		assert self.child in self.child
		assert self.other not in self.child
		assert self.another not in self.child

		assert self.parent not in self.other
		assert self.child not in self.other
		assert self.other in self.other
		assert self.another in self.other

		assert self.parent not in self.another
		assert self.child not in self.another
		assert self.other not in self.another
		assert self.another in self.another

	def test_str(self):
		assert str(self.parent) == '1.A'
		assert str(self.child) == '1.A.1'
		assert str(self.longone) == '1.A.1.1.1-ACC123'

	def test_repr(self):
		assert repr(self.child) == 'TCID("1.A.1")'

	def test_indexing(self):
		assert self.child[:2] == self.parent
		assert self.longone[:3] == self.child
		assert self.longone[:2] == self.child[:2] == self.parent

	def test_comparison(self):
		shouldnotgofirst = kevtools_common.types.TCID('1.A.10')
		shouldgofirst = kevtools_common.types.TCID('1.A.2')

		assert shouldgofirst < shouldnotgofirst
		assert self.parent < self.child
		assert self.parent < self.another
		assert self.another > self.parent
		assert self.child < self.other

	def test_sort(self):
		TCID = kevtools_common.types.TCID
		assert TCID('2.A.1.3.2') < TCID('2.A.1.10.1')

if __name__ == '__main__':
	unittest.main()
