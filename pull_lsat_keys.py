#!/usr/bin/env python
from __future__ import print_function, division
import argparse


def main(fh, keys):
	out = ''
	table = []
	for l in fh:
		if keys[0] in l:
			table.append([])
		for k in keys:
			if k in l: table[-1].append(l.strip().split()[-1])
	return table

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('-k', nargs='+', required=True, help='Keys to pull')

	parser.add_argument('infile', nargs='?', default='/dev/stdin', help='LSAT output to read (default: stdin)')

	args = parser.parse_args()


	with open(args.infile) as f:
		table = main(f, keys=args.k)

		print('\n'.join(['\t'.join(row) for row in table]))
