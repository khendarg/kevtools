# TSViss Army Knife

Useful tools for manipulating TSVs

## cat

The `cat` subcommand simply concatenates files.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

## color

Apply conditional coloring to TSVs with numeric data. Requires matplotlib and assumes the existence of a column with adequately scaled data.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`--cmap COLORMAP`: (str, optional): Color map to use. Defaults to viridis.

`-k COL1,COL2,COL3`: (\*int, optional): Comma-separated list of 1-indexed column ranges to color. Defaults to all columns containing numeric data.

`--vmin VMIN`: (float, optional): Manually set the low end of the gradient. Switches to line-buffered output if used in conjuction with `--vmax`, which may be useful for large datasets.

`--vmax VMAX`: (float, optional): Manually set the high end of the gradient. Switches to line-buffered output if used in conjuction with `--vmin`, which may be useful for large datasets.

## cut

Cut files. Allows duplicate/non-monotonically increasing column IDs.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.

`-f`: (\*int, optional): Which columns to keep.

`-s`: Remove undelimited lines instead.

## hstack

Concatenate TSVs horizontally. Accepts only tables with equal heights and without ragged right edges by default.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.

`-f`: (str, optional): Fill empty cells with this string if set. Otherwise, mismatched table heights will result in crashes.

`--skip`: (str, optional): Columns to exclude in non-initial tables. Useful for skipping identical row labels provided all tables are in the same order.

## matrix

Convert TSVs into matrices or vice versa.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.
`--header-line`: (int, optional): 1-indexed line to use as a header. If set, tsviss\_army\_knife will skip lines preceding the header line.

### TSV-to-matrix arguments

`-m`: (required): Convert a TSV into a matrix or series of matrices.

`-k`: (int[2], optional): 1-indexed columns to use as matrix indices in row-column order. Defaults to 1 and 2.

### Matrix-to-TSV argumentts

`-t`: (required): Convert a matrix or series of matrices into a TSV.

## shape

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.

`--per-line`: (optional): Output per-line stats

## sql

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.

`-c`: (str, optional): SQLite commands to execute. If the first line in the input isn't a #-delimited comment row of column names, tsviss\_army\_knife will name the columns c1, c2, c3, ... cn.

`--ignore-errors`: (optional): Ignore row size mismatch errors

`--header-line`: (int, optional): Line to use as a header. If set, tsviss\_army\_knife will skip lines preceding the header line.

`--ignore-column-names`: (optional): Disable column name autodetection in favor of c1...cn.

### SQL details

Any TSV loaded with this command is inserted into the table "tsv".  

Column names are taken from the TSV where possible and assigned as c1, c2, c3, ... cn otherwise.

## tcsort

Sort TSVs according to TC-ID.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.

`-k`: (\*int, optional): Comma-separated lists of ranges of 1-indexed columns containing TC-IDs to use as sort keys.

## transpose

Transpose TSVs.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.

`--clone`: (optional): Fill empty fields according to contents in transpose/original.

## uniq

Collect unique entries. Unlike POSIX uniq, this picks up on multicolumn uniqueness keys (e.g. the combination of qacc/sacc in BLAST TSVs) and nonconsecutive duplicates.

### Positional arguments

`infile` (str, optional): Files to concatenate. Defaults to stdin.

### Optional arguments

`-c`: (optional): Count occurrences.

`-d`: (str, optional): Field delimiter to use. Defaults to <TAB>.

`-k`: Comma-separated list of ranges of 1-indexed columns containing TC-IDs.
