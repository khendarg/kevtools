#!/usr/bin/env python

import argparse

def reshape_tsv(fh, headers=True, key=2, skipfirst=False):
	allcats = set()
	raveled = {}
	first = True
	for l in fh:
		if l.startswith('#'): continue
		elif not l.strip(): continue
		elif skipfirst and first:
			first = False
			continue
		else:
			sl = l.split('\t')

			cat1, cat2 = sl[:2]
			allcats.add(cat1)
			allcats.add(cat2)
			val = sl[key].replace('\n', '')
			raveled[cat1,cat2] = val

	table = []
	if headers:
		table.append([''])
		for cat1 in sorted(allcats):
			table[-1].append(cat1)
	for cat1 in sorted(allcats):

		table.append([])
		if headers: table[-1].append(cat1)
		for cat2 in sorted(allcats):
			table[-1].append(raveled.get((cat1,cat2), ''))

	return table
			


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='*', default=['/dev/stdin'], help='TSVs')
	parser.add_argument('-k', type=int, default=3, help='1-indexed column to put in table')
	parser.add_argument('-v', action='store_true', help='Invert results, i.e. return matched lines')
	parser.add_argument('--skip-first', action='store_true', help='Skip first row')

	args = parser.parse_args()

	key = args.k - 1

	for fn in args.infile:
		with open(fn) as fh:
			arr = reshape_tsv(fh, key=key, skipfirst=args.skip_first)
			for row in arr:
				print('\t'.join(row))
