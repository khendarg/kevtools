#!/usr/bin/env python
from __future__ import print_function, unicode_literals

import sys
if sys.version.startswith('3'): from urllib.request import urlopen
else: from urllib2 import urlopen
import re
import tempfile
import argparse
from kevtools_common.types import TCID

def parse_superfampage(fh, parse_tcid=False):
	currentsuperfam = None
	superfamdict = {}
	for ul in fh:
		try: l = ul.decode('utf-8')
		except AttributeError: l = ul

		if '<h3>' in l:
			sf = re.findall('<h3>([^<]+)</h3>', l)
			if not sf: continue
			currentsuperfam = sf[0]
			superfamdict[currentsuperfam] = set()

		elif currentsuperfam:
			linefams = re.findall('[0-9]\.[A-Z]\.[0-9]+', l)
			if linefams: 
				for fam in linefams: 
					if parse_tcid: superfamdict[currentsuperfam].add(TCID(fam))
					else: superfamdict[currentsuperfam].add(fam)
	return superfamdict

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infile', nargs='?', help='Downloaded TCDB superfamilies page (if available). Defaults to downloading the TCDB superfamilies page.')

	args = parser.parse_args()

	if args.infile is None:
		fh = tempfile.TemporaryFile()
		fh.write(urlopen('http://tcdb.org/superfamily.php').read())
		fh.seek(0)
	else: fh = open(args.infile)

	superfamdict = parse_superfampage(fh)
	for sf in superfamdict: print('{}\t{}'.format(sf, ' '.join([fam for fam in sorted(superfamdict[sf])])))
