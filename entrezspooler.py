#!/usr/bin/env python

import argparse
import sys
from urllib.request import urlopen

parser = argparse.ArgumentParser()

parser.add_argument('urlprefix')
parser.add_argument('--postdata', required=True)
parser.add_argument('infile', nargs='?', default=sys.stdin, type=argparse.FileType('r'))
parser.add_argument('-O', type=argparse.FileType('w'), required=True)
parser.add_argument('--batch', type=int, default=1000, help='Size of each request in IDs/accessions')

args = parser.parse_args()

idlist = []
for l in args.infile:
    idlist.append(l.strip())

for index in range(0, len(idlist), args.batch):
    idstr = ','.join(idlist[index:index+args.batch])
    postdata = '{}&id={}'.format(args.postdata, idstr).encode('iso-8859-1')

    with urlopen(args.urlprefix, data=postdata) as fh:
        args.O.write(fh.read().decode('iso-8859-1'))

