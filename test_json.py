#!/usr/bin/env python

import json
import argparse
import pathlib
import os
import sys
import gzip

messages = {0: "OK",
	1: "SyntaxError",
	2: "FileNotFound",
	-1: "UnknownError",
}
messages_color = {0: "\033[32mOK\033[0m",
	1: "\033[31mSyntaxError\033[0m",
	2: "\033[31mFileNotFound\033[0m",
	-1: "\033[31mUnknownError\033[0m",
}

def test_json(fp):
	fh = None
	try:
		if fp.suffix.lower().endswith("gz"):
			fh = gzip.open(fp, "rt")
		else:
			fh = open(fp, "rt")
		try: json.load(fh)
		except json.decoder.JSONDecodeError as e: 
			return 1, e
		except Exception as e: 
			return -1, e
	except FileNotFoundError as e: 
			return 2, e
	finally: 
		if fh is not None: fh.close()
	return 0, ""

def main(args):
	if args.color.lower() == "auto": color = os.isatty(sys.stdout.fileno())
	elif args.color.lower() == "always": color = True
	elif args.color.lower() == "never": color = False
	else: raise ValueError("Unknown color option: '{}'".format(args.color))

	for fp in args.infile:
		result, detail = test_json(fp)
		out = "{}".format(fp)
		if color: out += "\t{}".format(messages_color[result])
		else: out += "\t{}".format(messages[result])
		out += "\t{}".format(detail)
		print(out)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Test JSON for parseability")

	parser.add_argument("--color", default="auto", help="When to color output. Options: auto, always, never. (default: auto)")
	parser.add_argument("infile", nargs="*", default=[pathlib.Path("/dev/stdin")], type=pathlib.Path)

	args = parser.parse_args()

	main(args)
