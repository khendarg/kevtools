#!/usr/bin/env python

import argparse
import pathlib
import re
import sys

from Bio import SeqIO

def grep(pattern, src, seqmode=False, invert=False, single=False):
    """ Print FASTA records that match patterns.

    Parameters
    ----------

    pattern : str
        This must be a valid regular expression in Python's regexp dialect.

    src : file-like object or iterable or str
        If src is a file-like object, a string, or a pathlib.Path object, 
        Bio.SeqIO.parse is called to create a Bio.SeqIO.SeqRecord generator. If
        src is an iterable, fasta_grepper will assume it contains
        Bio.SeqIO.SeqRecord objects and attempt to iterate over it.

    seqmode : bool
        Search inside sequences instead of sequence headers. Consider using a
        more robust sequence search tool (e.g. BLAST) for nontrivial queries.

    invert : bool
        Print non-matching sequences instead of matching sequences.

    single : bool
        Print the first matching sequence and exit. If src is lazily processed
        (e.g. file-like objects or generators), this may result in src not 
        being exhausted.

    Returns
    -------
    out : list
        A list of Bio.SeqIO.SeqRecord objects matching the supplied pattern.

    """
    record_iter = None

    if "readline" in dir(src):
        record_iter = SeqIO.parse(src, "fasta")
    elif isinstance(src, str):
        record_iter = SeqIO.parse(src, "fasta")
    elif isinstance(src, pathlib.Path):
        record_iter = SeqIO.parse(src, "fasta")
    elif "__iter__" in dir(src):
        record_iter = iter(src)
    else:
        raise TypeError("Unknown format for src ({})".format(type(src)))
    recording = False

    results = []
    for record in record_iter:
        addme = False
        if seqmode:
            if invert ^ bool(re.search(pattern, str(record.seq))): addme = True
        else:
            if record.name and not record.description: searchme = record.name
            elif record.description and not record.name: searchme = record.description
            elif record.name and record.description: searchme = record.name
            else: searchme = ''

            if invert ^ (bool(searchme) and bool(re.search(pattern, searchme))): addme = True
            #elif invert ^ (bool(record.description) and bool(re.search(pattern, record.description))): addme = True
        if addme: 
            results.append(record)
            if single: break
    return results

def main(*args, **kwargs):
    """ Alias for grep() for backward compatibility 

    See Also
    --------
    grep : Print FASTA records that match patterns.
    """
    grep(*args, **kwargs)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', action='store_true', help='Count matches')
    parser.add_argument('-f', action='store_true', help='Treat PATTERN as a filename containing newline-separated patterns')
    parser.add_argument('-o', action='store_true', help='Retrieve only the accessions (and not the sequences)')
    parser.add_argument('-s', action='store_true', help='Sequence grep mode, i.e. grep in sequences rather than in headers')
    parser.add_argument('-v', action='store_true', help='Inverse match')
    parser.add_argument('--single', action='store_true', help='Stop at first match')
    parser.add_argument('pattern', help='Pattern to grep for')
    parser.add_argument('infile', nargs='*', default=['/dev/stdin'], help='Files to read in (default: stdin)')

    args = parser.parse_args()

    if not args.f: pattern = args.pattern
    else: 
        patternlist = []
        with open(args.pattern) as fh:
            for l in fh: patternlist.append(l.replace('\n', ''))
        pattern = '|'.join(patternlist)

    for fn in args.infile:

        with open(fn) as f: results = grep(pattern, f, seqmode=args.s, invert=args.v, single=args.single)
        out = ''

        if args.c: print(len(results))
        else:
            if args.o:
                for record in results:
                    if seq.id: print(seq.id)
                    elif seq.name: print(seq.name)
                    elif seq.description: print(seq.description)
                    else: print()
            else: SeqIO.write(results, sys.stdout, "fasta")
