#!/usr/bin/env python

import argparse
import get_superfamilies
from kevtools_common.types import TCID
import sys

def precluster(tclist, superfams, rewardtable=None):
	rewardtable = None
	if rewardtable is None: rewardtable = {}
	rewards = {
		'nosuperfamily':rewardtable.get('nosuperfamily', -0.02),
		'samesuperfamily':rewardtable.get('samesuperfamily', -0.15),
		'insuperfamily':rewardtable.get('insuperfamily', -0.01),
		'unknownsuperfamily':rewardtable.get('unknownsuperfamily', 0.07),
		'class':rewardtable.get('class', -0.01),
		'subclass':rewardtable.get('subclass', -0.01),
		'family':rewardtable.get('family', -0.01),
		'subfamily':rewardtable.get('subfamily', -0.01),
		'system':rewardtable.get('system', -0.0)
	}
	rewards = {
		'nosuperfamily':rewardtable.get('nosuperfamily', -0.82),
		'samesuperfamily':rewardtable.get('samesuperfamily', -0.95),
		'insuperfamily':rewardtable.get('insuperfamily', -0.81),
		'unknownsuperfamily':rewardtable.get('unknownsuperfamily', -0.73),
		'class':rewardtable.get('class', -0.01),
		'subclass':rewardtable.get('subclass', -0.01),
		'family':rewardtable.get('family', -0.01),
		'subfamily':rewardtable.get('subfamily', -0.01),
		'system':rewardtable.get('system', -0.0)
	}
	rewards = {
		'nosuperfamily':rewardtable.get('nosuperfamily', -0.82),
		'samesuperfamily':rewardtable.get('samesuperfamily', -0.90),
		'insuperfamily':rewardtable.get('insuperfamily', -0.76),
		'unknownsuperfamily':rewardtable.get('unknownsuperfamily', -0.73),
		'class':rewardtable.get('class', -0.02),
		'subclass':rewardtable.get('subclass', -0.02),
		'family':rewardtable.get('family', -0.02),
		'subfamily':rewardtable.get('subfamily', -0.02),
		'system':rewardtable.get('system', -0.0)
	}
	fam2sf = {}

	for sfname in superfams:
		for fam in superfams[sfname]:
			fam2sf[fam] = sfname

	query2sf = {}
	for tcid in tclist:
		if tcid[:3] in fam2sf:
			query2sf[tcid] = fam2sf[tcid[:3]]
		elif tcid[:4] in fam2sf:
			query2sf[tcid] = fam2sf[tcid[:4]]
		elif tcid[:5] in fam2sf:
			query2sf[tcid] = fam2sf[tcid[:5]]
		else: query2sf[tcid] = None

	sf2query = {}
	for query in query2sf: 
		if query2sf[query] in sf2query: sf2query[query2sf[query]].append(query)
		else: sf2query[query2sf[query]] = [query]
	for sfname in sf2query:
		print(sfname, sf2query[sfname], file=sys.stderr)

	distances = {}
	for tcid1 in sorted(tclist):
		for tcid2 in sorted(tclist):
			distance = 1.0
			if tcid1 == tcid2: distance = 0.0
			else:
				if (query2sf[tcid1] is None) and (query2sf[tcid2] is None): distance += rewards['nosuperfamily']
				elif query2sf[tcid1] == query2sf[tcid2]: distance += rewards['samesuperfamily']
				elif (query2sf[tcid1] is not None) and (query2sf[tcid2] is not None): distance += rewards['insuperfamily']
				elif (query2sf[tcid1] is None) ^ (query2sf[tcid2] is None): distance += rewards['unknownsuperfamily']

				if (query2sf[tcid1] is None) or (query2sf[tcid2] is None):
					if tcid1[:1] == tcid2[:1]: distance += 0.5 * rewards['class']
					if tcid1[:2] == tcid2[:2]: distance += 0.5 * rewards['subclass']
					if tcid1[:3] == tcid2[:3]: distance += 0.5 * rewards['family']
					if tcid1[:4] == tcid2[:4]: distance += 0.5 * rewards['subfamily']
					if tcid1[:5] == tcid2[:5]: distance += 0.5 * rewards['system']
				else:
					if tcid1[:1] == tcid2[:1]: distance += rewards['class']
					if tcid1[:2] == tcid2[:2]: distance += rewards['subclass']
					if tcid1[:3] == tcid2[:3]: distance += rewards['family']
					if tcid1[:4] == tcid2[:4]: distance += rewards['subfamily']
					if tcid1[:5] == tcid2[:5]: distance += rewards['system']

			distances[(tcid1,tcid2)] = distance

	return distances

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='?', default='/dev/stdin', help='List of TC-IDs to cluster (default:stdin)')

	parser.add_argument('--reward-superfam', type=float, default=-0.5, help='Distance modifier between superfamilies (default:-0.5)')
	parser.add_argument('--reward-class', type=float, default=0.0, help='Distance modifier between TC-classes (default:-0.0)')
	parser.add_argument('--reward-subclass', type=float, default=-0.1, help='Distance modifier between TC-subclasses (default:-0.2)')
	parser.add_argument('--reward-family', type=float, default=-0.1, help='Distance modifier between TC-families (default:-0.0)')
	parser.add_argument('--reward-subfamily', type=float, default=-0.1, help='Distance modifier between TC-subfamilies (default:-0.0)')

	parser.add_argument('--superfamilies', help='Superfamilies page (if available)')

	args = parser.parse_args()

	rewards = {'reward_superfamily':args.reward_superfam,
		'reward_class':args.reward_class,
		'reward_subclass':args.reward_subclass,
		'reward_family':args.reward_family,
		'reward_subfamily':args.reward_subfamily
	}

	if args.superfamilies is None: raise NotImplementedError
	else: superfamfh = open(args.superfamilies)
	superfams = get_superfamilies.parse_superfampage(superfamfh, parse_tcid=True)
	acclist = list(set([TCID(l.strip()) for l in open(args.infile) if l.strip()]))
	distances = precluster(acclist, superfams, rewardtable=rewards)

	for tcid1, tcid2 in distances: print('{}\t{}\t{}'.format(tcid1, tcid2, distances[(tcid1,tcid2)]))
