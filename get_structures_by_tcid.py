#!/usr/bin/env python
from __future__ import print_function, division
import sys
import os
import argparse

from kevtools_common.types import TCID, TCIDCollection


class Stuff(object):
	tmdatadir = os.environ.get('TMDATA', None)

	def main(self, args):
		parser = argparse.ArgumentParser()

		if self.tmdatadir: parser.add_argument('--tmdatadir', default=self.tmdatadir, help='TMdata directory (default: $TMDATA, which == {})'.format(self.tmdatadir))
		else: parser.add_argument('--tmdatadir', default='tmdata', help='TMdata directory (default: ./tmdata ($TMDATA is unset)')

		parser.add_argument('--simple', action='store_true', help='Simply print PDBs')

		parser.add_argument('parents', metavar='parent_tcid', nargs='+', help='TCIDs to grab structure listings for')

		params = parser.parse_args(args)

		parents = TCIDCollection()

		for tcid in params.parents:
			parents.add_tcid(tcid)


		keepme = {}
		with open('{}/tcmap.tsv'.format(params.tmdatadir)) as f:
			for l in f:
				if not l.strip(): continue
				elif l.lstrip().startswith('#'): continue

				sl = l.strip().split('\t')
				acc = TCID(sl[0])
				pdbs = sl[1]
				found = False

				for parent in parents:
					if acc in parent:
						keepme[acc] = l.strip()
						found = True
						break

				if found: continue
		if params.simple:
			for k in sorted(keepme):
				l = keepme[k].split('\t')[1]
				for acc in l.split(','): print(acc)
		else: 
			for k in sorted(keepme): print(keepme[k])

if __name__ == '__main__':
	x = Stuff()
	x.main(sys.argv[1:])
