#!/usr/bin/env python

from PIL import Image
import curses
import argparse
import os


class Clivia(object):
	paths = None
	stdscr = None
	cached = None
	imagepointer = 0

	aspect_correction = 2.5
	max_colors = 240

	running = True

	def __init__(self):
		self.paths = []
		self.cached = {}

	def add_paths(self, *paths):
		for path in paths:
			if os.path.isfile(path): self.paths.append(path)
			elif os.path.isdir(path):
				for bn in sorted(os.listdir(path)):
					if os.path.isfile('{}/{}'.format(path, bn)): 
						try: 
							Image.open('{}/{}'.format(path, bn))
							self.paths.append('{}/{}'.format(path, bn))
						except OSError: continue

	def main(self, stdscr):
		self.stdscr = stdscr

		if not self.paths: return

		while self.running:
			self.launch_viewer()

	def launch_viewer(self):
		image = self.get_current_image()

		height, width = self.stdscr.getmaxyx()
		viewerwindow = curses.newwin(height, width, 0, 0)
		basename = self.paths[self.imagepointer]
		print('\x1b]2;{name}\x07'.format(name=basename))
		viewer = ViewerWindow(self, image, viewerwindow, aspect_correction=self.aspect_correction, max_colors=self.max_colors)
		try: viewer.run()
		finally:
			viewerwindow.clear()
			viewerwindow.refresh()


		#self.stdscr.addstr(str(image))
		#self.stdscr.addstr(str(dir(self.stdscr)))
		#self.stdscr.addstr(str(self.stdscr.getmaxyx()))
		#self.stdscr.getch()
	

	def get_current_image(self):
		self.imagepointer = self.imagepointer % len(self.paths)
		if self.paths[self.imagepointer] in self.cached:
			return self.cached[self.paths[self.imagepointer]]
		else:
			image = Image.open(self.paths[self.imagepointer])
			self.cached[self.paths[self.imagepointer]] = image
			return image

	def run(self):
		curses.wrapper(self.main)

class ViewerWindow(object):
	subcharacter = False
	zoom = 1
	def __init__(self, app, image, window, aspect_correction=2.5, max_colors=240):
		self.app = app
		self.image = app.get_current_image()
		self.max_colors = min(max_colors, 240)
		self.window = window
		self.aspect_correction = aspect_correction

		self.pos = [0, 0]

	def run(self):
		self.display()

		prevkeys = set([ord('b')])
		#set([ord('h'), ord('k'), curses.KEY_LEFT, curses.KEY_UP])
		nextkeys = set([ord('n')])
		#set([ord('l'), ord('j'), curses.KEY_RIGHT, curses.KEY_DOWN])
		helpkeys = set([ord('?'), curses.KEY_F1])
		while 1:
			k = self.window.getch()
			if k == ord('q'): 
				self.app.running = False
				return
			elif k in nextkeys:
				self.app.imagepointer += 1
				return
			elif k in prevkeys:
				self.app.imagepointer -= 1
				return
			elif k in helpkeys:
				self.show_help()
				self.display()
			elif k == ord('z'):
				self.zoom *= 2
				self.pos[0] = int(round(2 * self.pos[0]))
				self.pos[1] = int(round(2 * self.pos[1]))
				self.display()
			elif k == ord('x'):
				self.zoom *= 0.5
				self.pos[0] = int(round(0.5 * self.pos[0]))
				self.pos[1] = int(round(0.5 * self.pos[1]))
				self.display()
			elif k == ord('c'):
				self.zoom = 1
				self.pos = [0, 0]
				self.display()
			elif k == ord('h'):
				self.pos[1] -= 2
				self.display()
			elif k == ord('j'):
				self.pos[0] += 2
				self.display()
			elif k == ord('k'):
				self.pos[0] -= 2
				self.display()
			elif k == ord('l'):
				self.pos[1] += 2
				self.display()

		#self.window.addstr(str(dir(self.image)))
		#self.window.addstr(str((windowheight, windowwidth)))
		#self.window.addstr(str(self.image.size))
		#self.window.addstr(str(newimage.size))
		#self.window.getch()

	def display(self):
		self.window.clear()
		windowheight, windowwidth = self.window.getmaxyx()
		if self.subcharacter: 
			windowheight *= 2
			windowwidth *= 2
		imagewidth, imageheight = self.image.size

		#if imagewidth < windowwidth and imageheight < windowheight:
		#	#no scaling needed
		#	self.blit(self.image.quantize(self.max_colors))

		imagewidth *= self.aspect_correction

		widthratio = imagewidth / windowwidth
		heightratio = imageheight / windowheight

		if widthratio >= heightratio:
			newimagewidth = windowwidth
			newimageheight = imageheight / widthratio
		elif widthratio < heightratio:
			newimageheight = windowheight
			newimagewidth = imagewidth / heightratio

		newimagewidth *= self.zoom
		newimageheight *= self.zoom

		newimagewidth = int(round(newimagewidth))
		newimageheight = int(round(newimageheight))

		newimage = self.image.resize((newimagewidth, newimageheight)).quantize(self.max_colors)

		self.blit(newimage)


	def show_help(self):
		helpwin = curses.newwin(18, 25, 4, 4)
		helpwin.addstr('''FILE NAVIGATION:
================
b - Previous file
n - Next file
q - Quit

IMAGE NAVIGATION
================
c - Reset view
h - Move up
j - Move down
k - Move left
l - Move right
x - Zoom out
z - Zoom in

Press any key to return
''')
		helpwin.getch()
		helpwin.clear()
		del helpwin

	def blit(self, image):
		width, height = image.size
		windowheight, windowwidth = self.window.getmaxyx()

		palette = image.getpalette()
		colordict = {}
		j = 9
		k = 0
		for i in range(0, self.max_colors*3, 3):
			#colordict[tuple(palette[i:i+3])] = j
			colordict[k] = j
			curses.init_color(j, palette[i]*3, palette[i+1]*3, palette[i+2]*3)
			curses.init_pair(j, 0, j)
			j += 1
			k += 1

		#self.window.addstr(str(colordict))
		#self.window.getch()
		#self.window.clear()

		for windowy, imagey in enumerate(range(self.pos[0], self.pos[0] + windowheight)):
			if imagey >= height: continue
			if windowy >= windowheight-1: continue
			for windowx, imagex in enumerate(range(self.pos[1], self.pos[1] + windowwidth)):
				if imagex >= width: continue 
				if windowx >= windowwidth-1: continue
				elif imagey < 0 or imagex < 0: pixel = 255
				else: pixel = image.getpixel((imagex, imagey))

				self.window.addstr(windowy, windowx, ' ', curses.color_pair(colordict.get(pixel, 0)))

		#for windowy, y in enumerate(range(self.pos[0], min(windowheight-1, height))):
		#	for windowx, x in enumerate(range(self.pos[1], min(windowwidth-1, width))):
		#		if x < 0 or y < 0: pixel = 255
		#		else: pixel = image.getpixel((x, y))
		#		self.window.addstr(windowy, windowx, ' ', curses.color_pair(colordict.get(pixel, 0)))
		#		#row += '\033[41m '
		#		#row += '.' if x else '^'
		#		#row += '\033[48;2;{};{};{}m '.format(*image.getpixel((x,y)))
		self.window.refresh()

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infiles', nargs='*', default=['.'], help='Files to view')
	if 'CLIVIA_ASPECT' in os.environ: parser.add_argument('--aspect', type=float, default=float(os.environ['CLIVIA_ASPECT']), help='Aspect ratio correction for nonsquare terminal fonts. Default: $CLIVIA_ASPECT == {}'.format(os.environ['CLIVIA_ASPECT']))
	else: parser.add_argument('--aspect', type=float, default=2.5, help='Aspect ratio correction for nonsquare terminal fonts. Reads the environment variable $CLIVIA_ASPECT if set. Default: 2.5')
	parser.add_argument('--max-colors', type=int, default=240, help='Maximum colors to use in 256-color mode. Default: 240')
	args = parser.parse_args()

	app = Clivia()
	app.add_paths(*args.infiles)
	app.aspect_correction = args.aspect
	app.max_colors = args.max_colors
	app.run()
