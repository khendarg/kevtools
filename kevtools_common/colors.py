import hashlib


def colorhash(s, salt="", hashfn=None, S=None, V=None, outfmt=None):
    """
    s: String to colorhash
    salt: Extra data, cf. PRNG seeds
    hashfn: Callback to str->bytes hash function. Defaults to hashlib.md5 (which is broken but OK because this is purely for aesthetics)
    S: If set, saturation
    V: If set, value
    outfmt: Format to output colors in. (float3 (default), int3, bytes, hex)

    Returns
    
    """
    hashfn = hashlib.md5() if hashfn is None else hashfn

    hashfn.update(str(s).encode("utf-8"))
    hashfn.update(str(salt).encode("utf-8"))

    b = hashfn.digest()

    H = (b[0] + (b[1] << 8)) % 360
    S = max(b[5], b[6]) / 255 if S is None else S
    #V = min(max(b[10], b[11]), max(b[12], b[13]), max(b[14], b[15])) / 255 if V is None else V
    V = max(b[10], b[12], b[14]) / 255 if V is None else V

    C = V * S
    Hprime = H / 60
    X = C * (1 - abs((H % 2) - 1))

    if Hprime < 1: R, G, B = C, X, 0.
    elif Hprime < 2: R, G, B = X, C, 0.
    elif Hprime < 3: R, G, B = 0., C, X
    elif Hprime < 4: R, G, B = 0., X, C
    elif Hprime < 5: R, G, B = X, 0., C
    else: R, G, B = C, 0., X
    m = V - C
    R += m
    G += m
    B += m
    if outfmt is None or outfmt == "float3": return R, G, B
    else:
        R = int(255 * R)
        G = int(255 * G)
        B = int(255 * B)
    if outfmt == "int3": return (R, G, B)
    elif outfmt == "bytes": return bytes([R, G, B])
    elif outfmt == "hex": return "#{:02x}{:02x}{:02x}".format(R, G, B)
    else: raise ValueError("Unknown outfmt: {}".format(repr(outfmt)))
