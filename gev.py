#!/usr/bin/env python

import argparse
import scipy.stats
import numpy as np
import pathlib
import sys
import matplotlib.pyplot as plt

class GEVViewer(object):
    def __init__(self, values, testvalue=None, bins=20, normalize=True):
        self.values = values
        self.testvalue = testvalue
        self.bins = bins
        self.normalize = normalize

        self.mode = None
        self.pvalpoint = None
        self.histogram = None

    def main(self, outfile=None):
        statistics = {}
        self.rawshortgevparams = scipy.stats.genextreme.fit(self.values)
        self.kde = scipy.stats.gaussian_kde(self.values)

        a = b = 1
        r = np.sum(self.values >= self.testvalue)
        beta = scipy.stats.beta(a+r, a+len(self.values)-r)
        pbeta = beta.ppf(0.999)

        statistics["shortgev-p-value"] = 1 - scipy.stats.genextreme.cdf(self.testvalue, *self.rawshortgevparams)
        statistics["gaussian-p-value"] = self.kde.integrate_box_1d(self.testvalue, 9e99)
        statistics["beta-p-value"] = pbeta
        statistics["rank"] = sum(self.values >= self.testvalue) + 1
        statistics["mean"] = np.mean(self.values)
        statistics["stdev"] = np.std(self.values)
        statistics["quartiles"] = [np.min(self.values), np.percentile(self.values, 25), np.median(self.values), np.percentile(self.values, 75), np.max(self.values)]
        statistics["z-score"] = (self.testvalue - statistics["mean"]) / statistics["stdev"]
        print("#KDE p-value: {gaussian-p-value:0.1e}".format(**statistics))
        print("#GEV p-value: {shortgev-p-value:0.1e}".format(**statistics))
        print("#Beta p-value: {beta-p-value:0.1e}".format(**statistics))
        print("#rank: {rank}".format(**statistics))
        print("#quartiles: {quartiles}".format(**statistics))
        print("#Z-score: {z-score:0.1f} ({mean:0.1f}+/-{stdev:0.1f})".format(**statistics))
        print()
        print("#Click and drag to compute p-values")
        print("#Use [?] to get help on GEVViewer shortcuts")



        self.fig, self.ax = plt.subplots()
        self.fig.canvas.mpl_connect("button_press_event", self.onmousedown)
        self.fig.canvas.mpl_connect("button_release_event", self.onmouseup)
        self.fig.canvas.mpl_connect("motion_notify_event", self.onmousemove)
        self.fig.canvas.mpl_connect("key_press_event", self.onkeydown)

        X = np.arange(np.min(self.values), np.max(self.values), (np.max(self.values) - np.min(self.values)) * 0.001)
        Y = self.kde.evaluate(X)

        self.ax.plot(X, Y, label="KDE")
        self.ax.plot(X, scipy.stats.genextreme.pdf(X, *self.rawshortgevparams), label="GEV distribution")
        gev_t = scipy.stats.genextreme.pdf(self.testvalue, *self.rawshortgevparams)
        self.ax.scatter(self.testvalue, gev_t, label="p({X:0.1f}, {Y:0.1e}) = {p:0.1e}\nBeta P-value (0.999, r={r}) = {pbeta:0.1e}".format(X=self.testvalue, Y=gev_t, p=statistics["shortgev-p-value"], r=r, pbeta=pbeta))

        self.plot_histogram()
        #self.ax.annotate("p({X:0.1f}, {Y:0.1e}) = {p:0.1e}".format(X=args.t, Y=gev_t, p=statistics["shortgev-p-value"]), (args.t, gev_t))
        self.ax.legend()

        if outfile: self.fig.savefig(outfile)
        else: plt.show()

    def onkeydown(self, event):
        if event.key == "[": 
            self.bins -= 1
            if self.bins == 0: self.bins -= 1
            self.plot_histogram()
        elif event.key == "]": 
            self.bins += 1
            if self.bins == 0: self.bins += 1
            self.plot_histogram()
        elif event.key == "n": 
            self.normalize = not self.normalize
            self.plot_histogram()
        elif event.key == "?":
            self.print_help()

    def print_help(self):
        print("""
CONTROLS
========

Keyboard:
[ - Decrease --bins by 1
] - Increase --bins by 1
n - Toggle normalization mode

Mouse:
Click and drag: Add and plot new test value
""", file=sys.stderr)

    def plot_histogram(self):
        if self.histogram is not None: 
            try: self.histogram.remove()
            except ValueError: pass

        if self.bins > 0:
            histcounts, histbins = np.histogram(self.values, bins=int(self.bins))
            histwidth = histbins[1] - histbins[0]

        else:
            histcounts, histbins = np.histogram(self.values, bins=np.arange(np.min(self.values), np.max(self.values) + abs(self.bins), abs(self.bins)))
            histwidth = abs(self.bins)

        label = "Histogram ({} {:0.2f}-wide bins)".format(len(histcounts), histwidth)

        curarea = np.sum(histcounts * histwidth)
        normcounts = histcounts / curarea
        if self.normalize: histY = normcounts
        else: histY = histcounts

        histX = (histbins[:-1] + histbins[1:]) / 2
        histYkde = self.kde.evaluate(histX)

        self.histogram = self.ax.bar(histX, histY, histwidth, label=label, zorder=-1, color="tab:green")
        self.ax.legend()
        self.fig.canvas.draw()
        self.fig.canvas.draw()

    def onmousedown(self, event):
        if self.mode is None:
            if event.inaxes is self.ax:
                self.mode = "pvaldrag"
                if self.pvalpoint is not None: self.pvalpoint.remove()
                self.pvalx = event.xdata
                self.pvaly = scipy.stats.genextreme.pdf(self.pvalx, *self.rawshortgevparams)

                a = b = 1
                r = np.sum(self.values >= self.pvalx)
                beta = scipy.stats.beta(a+r, a+len(self.values)-r)
                pbeta = beta.ppf(0.999)

                #self.pvalpoint = self.ax.scatter(self.pvalx, self.pvaly, color="tab:red", label="p({x:0.1f},{y:0.1e}) = {p:0.1e}".format(x=self.pvalx, y=self.pvaly, p=1 - scipy.stats.genextreme.cdf(self.testvalue, *self.rawshortgevparams)))

                self.pvalpoint = self.ax.scatter(self.pvalx, self.pvaly, color="tab:red", label="p({x:0.1f},{y:0.1f}) = {p:0.1e}\nBeta P-value (0.999, r={r}) = {pbeta:0.1e}".format(x=self.pvalx, y=self.pvaly, p=1 - scipy.stats.genextreme.cdf(self.pvalx, *self.rawshortgevparams), pbeta=pbeta, r=r))
                self.ax.legend()
                self.fig.canvas.draw()
        elif self.mode =="pvaldrag": pass

    def onmouseup(self, event):
        if self.mode is None: pass
        elif self.mode == "pvaldrag": self.mode = None

    def onmousemove(self, event):
        if self.mode is None: pass
        elif self.mode == "pvaldrag":
            if event.inaxes is self.ax:
                self.pvalpoint.remove()

                self.pvalx = event.xdata
                self.pvaly = scipy.stats.genextreme.pdf(self.pvalx, *self.rawshortgevparams)
                a = b = 1
                r = np.sum(self.values >= self.pvalx)
                beta = scipy.stats.beta(a+r, a+len(self.values)-r)
                pbeta = beta.ppf(0.999)

                self.pvalpoint = self.ax.scatter(self.pvalx, self.pvaly, color="tab:red", label="p({x:0.1f},{y:0.1f}) = {p:0.1e}\nBeta P-value (0.999, r={r}) = {pbeta:0.1e}".format(x=self.pvalx, y=self.pvaly, p=1 - scipy.stats.genextreme.cdf(self.pvalx, *self.rawshortgevparams), pbeta=pbeta, r=r))
                self.ax.legend()
                self.fig.canvas.draw()

def parse_tsv(fh, column=0, delimiter="\t"):
    out = []
    for l in fh:
        if l.strip().startswith("#"): continue
        elif not l.strip(): continue

        out.append(float(l.split(delimiter)[column]))
    return np.array(out)


def main(args):
    with open(args.infile) as fh:
        values = parse_tsv(fh, column=args.k)

    app = GEVViewer(values=values, testvalue=args.t, bins=args.bins, normalize=not args.f)
    app.main(outfile=args.o)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser("Generalized extreme value distribution tester")

    parser.add_argument("infile", nargs="?", type=pathlib.Path, default=pathlib.Path("/dev/stdin"), help="Input TSV")
    parser.add_argument("-k", type=int, default=0, help="Column containing numeric values (default: 0)")

    parser.add_argument("-t", type=float, required=True, help="Test value to compare against distribution")
    parser.add_argument("-b", "--bins", type=float, default=20, help="Number of bins. Use negative values to specify bin size instead.")
    parser.add_argument("-f", action="store_true", help="Don't normalize the histogram, plotting raw bin counts instead of fraction")
    parser.add_argument("-o", help="Where to save the resulting figure")

    args = parser.parse_args()

    main(args)
