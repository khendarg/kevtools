#!/usr/bin/env python

import argparse
import sys
import os
import xml.etree.ElementTree as ET

def get_sequences(fh):
	doc = ET.parse(fh)
	root = doc.getroot()

	sequences = {}
	id2name = {}
	for section in root:
		if section.tag.endswith('training_set'):
			for seq in section:
				if seq.tag.endswith('sequence'):
					id2name[seq.attrib['id']] = seq.attrib['name']
		elif section.tag.endswith('motifs'):
			for motif in section: 
				motifid = motif.attrib['id']
				sequences[motifid] = []
				for subsection in motif: 
					if subsection.tag.endswith('contributing_sites'):
						for site in subsection:
							siteseq = ''
							for prop in site:
								if prop.tag.endswith('site'):
									for letter in prop:
										siteseq += letter.attrib['letter_id']
							#print(siteseq)
							faname = '{}__{}'.format(motifid, id2name[site.attrib['sequence_id']])
							fasta = '>{}\n{}'.format(faname, siteseq)
							sequences[motifid].append(fasta)
	return sequences
							
						

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('infile')
	parser.add_argument('-o', '--outdir', help='Where to save sequences (default: write all to stdout)')

	args = parser.parse_args()

	if 'meme.xml' in args.infile: infmt = 'meme'
	else: print('[WARNING]: This does not look like MEME XML output', file=sys.stderr)

	with open(args.infile) as fh:
		sequences = get_sequences(fh)

		if args.outdir:
			if not os.path.isdir(args.outdir): os.mkdir(args.outdir)
			for motif in sequences:
				with open('{}/{}.faa'.format(args.outdir, motif), 'w') as outfh:
					for seq in sequences[motif]:
						outfh.write(seq + '\n')
		else:
			for motif in sequences:
				for seq in sequences[motif]:
					print(seq)
