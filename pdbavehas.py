#!/usr/bin/env python
from __future__ import print_function, division

import Bio.PDB
import Bio.SeqIO
import argparse
import numpy as np
import subprocess

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
import pymol
def shannon(posdict):
	conservations = []
	for i in sorted(posdict):
		total = 0
		for k in posdict[i]: total += posdict[i][k]
		n = 0
		for k in posdict[i]:
			p = posdict[i][k]/total
			n -= p * np.log2(p)
		conservations.append(n)
	return conservations

def get_posdict(records):
	posdict = {}
	for i, seq in enumerate(records):
		#if i not in posdict: posdict[i] = {}

		for j, resn in enumerate(seq):
			if j not in posdict: posdict[j] = {}

			if resn == '-': continue

			try: posdict[j][resn] += 1
			except KeyError: posdict[j][resn] = 1
	return posdict


def perplexity(posdict):
	conservations = []

	for i in sorted(posdict):
		total = 0
		highest = 0
		#vals = [1] + sorted(posdict[i].values())
		#conservations.append(vals[-1]/vals[-2])
		
		for k in posdict[i]:
			total += posdict[i][k]
			highest = max(highest, posdict[i][k])
		#conservations.append(highest/(total - highest))
		#additive smooth to stop the crash
		conservations.append(highest/(total - highest + 1))
	return conservations

try: CODE = Bio.PDB.protein_letters_3to1
except AttributeError: CODE = Bio.PDB.to_one_letter_code

def load_pdb(pdbfn):
	p = Bio.PDB.PDBParser()
	struc = p.get_structure(pdbfn, pdbfn)
	seq = []
	for model in struc:
		for chain in model:
			for residue in chain:
				if residue.id[0].strip(): continue
				seq.append((CODE[residue.get_resname()], residue.id[1]))
	return seq
	
def needle(seq1, seq2):
	cmd = ['needle', '-asequence', 'asis:{}'.format(seq1),
			'-bsequence', 'asis:{}'.format(seq2), 
			'-gapopen', '10', '-gapextend', '0.5',
			'-out', 'stdout']
	out = subprocess.check_output(cmd)
	recording = False
	i = 0
	aln1 = ''
	aln2 = ''
	midline = ''
	for l in out.split('\n'):
		if l.startswith('asis'): recording = True
		if not recording: continue
		if l.startswith('#'): continue

		if i%4 == 0: aln1 += l[21:71]
		elif i%4 == 1: midline += l[21:71]
		elif i%4 == 2: aln2 += l[21:71]
		i += 1

	i1 = -1
	i2 = -1
	indices = []
	for resn1, midline, resn2 in zip(aln1, midline, aln2):
		if resn1 != '-': i1 += 1
		if resn2 != '-': i2 += 1

		if midline in ':|':
			indices.append((i1, i2))
	return indices

def get_color(z, zmin=-2, zmax=2, cmap='viridis', flip=False):
	x = np.clip(z, zmin, zmax)
	x = (x - zmin) / (zmax - zmin)
	cm = matplotlib.cm.get_cmap(cmap)
	if flip: colarray = cm(1-x)
	else: colarray = cm(x)
	r = '{:02x}'.format(int(round(colarray[0] * 255)))
	g = '{:02x}'.format(int(round(colarray[1] * 255)))
	b = '{:02x}'.format(int(round(colarray[2] * 255)))
	s = '0x{}{}{}'.format(r, g, b)
	return s
	

def main(f, match=None, pdbfn=None, zmin=-2, zmax=2, cmap='viridis', scaling='zscore'):
	records = Bio.SeqIO.parse(f, 'clustal')
	#conservations = shannon(records)
	posdict = get_posdict(records)
	conservations = perplexity(posdict)

	m = np.mean(conservations)
	s = np.std(conservations)
	z = lambda x: (x - m)/s

	f.seek(0)
	records = Bio.SeqIO.parse(f, 'clustal')
	seqcons = []
	for seq in records:
		if seq.description.startswith(match): 
			for resn, cons in zip(seq.seq, conservations):
				seqcons.append((resn, cons))
			break


	if pdbfn:
		if zmin < zmax:
			#for viridis and co.
			flip = True
		elif zmin > zmax:
			flip = False
			zmin, zmax = zmax, zmin
		else: raise ValueError('zmin should not be equal to zmax')

		pdbseq = load_pdb(args.pdb)

		#seqcons ::= [(resn, score), ...]
		seq1 = ''.join([resn for resn, stuff in seqcons])
		#pdbseq ::= [(resn, resi), ...]
		seq2 = ''.join([resn for resn, stuff in pdbseq])

		indices = needle(seq1, seq2)

		for i in range(1, len(sys.argv)): sys.argv.pop(-1)
		pymol.finish_launching()
		pymol.cmd.load(pdbfn)

		pymol.cmd.color('magenta')
		pymol.cmd.select('aligned', 'none')
		for i1, i2 in indices:
			#pymol.cmd.color(testcolors[i2%3], 'i. {}'.format(i2))
			if scaling == 'uniform': 
				color = get_color(seqcons[i1][1], zmin=min(conservations), zmax=max(conservations), cmap=cmap)
			else: color = get_color(z(seqcons[i1][1]), zmin=zmin, zmax=zmax, cmap=cmap, flip=flip)
			resi = pdbseq[i2][1]
			pymol.cmd.color(color, 'i. {}'.format(resi))
			pymol.cmd.select('aligned', 'aligned or i. {}'.format(resi))
		pymol.cmd.rotate('z', 360)
		pymol.cmd.deselect()
				
if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', default='/dev/stdin', help='A Clustal alignment')
	parser.add_argument('-p', help='A unique label corresponding to the aligned sequence closest to something on PDB')
	parser.add_argument('--pdb', help='Which PDB to draw on')
	parser.add_argument('--zmin', default=-2, type=float, help='Lowest Z-value in scale')
	parser.add_argument('--zmax', default=2, type=float, help='Highest Z-value in scale')
	parser.add_argument('--scaling', default='zscore', help='Scaling method (\033[1mzscore\033[0m, uniform)')
	parser.add_argument('--cmap', default='viridis', help='Matplotlib cmap to use')
	args = parser.parse_args()

	with open(args.infile) as f: main(f, match=args.p, pdbfn=args.pdb, zmin=args.zmin, zmax=args.zmax, cmap=args.cmap, scaling=args.scaling)
