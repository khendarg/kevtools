# kevtools

Assorted convenience scripts

## Summary

 - `autochop.py`: Automatically remove long/hydrophilic sections by average hydropathy/loop length

 - `cdsplit.py`: Explode CD-HIT results into one multirecord FASTA for each cluster

 - `circplot.py`: BLAST visualizer for circular genomes

 - `clivia.py`: Curses-based image viewer

 - `entrdbcmd.py`: Entrez wrappers

 - `entrezspooler.py`: Wrapper for running very large (>10k) Entrez queries

 - `explode_fasta.py`: Explode multirecord FASTAs into individual single record FASTAs

 - `fasta_grepper.py`: Grep implementation operating on sequence records instead of lines with matching capabilities both in the header and in the sequence

 - `filter_fastas_by_length.py`: See name

 - `get_clustalignment_stats.py`: Get CLUSTAL alignment stats

 - `get_fastalignment_stats.py`: ???

 - `get_structures_by_tcid.py`: Greps for TCIDs on deuterocol-formatted OPM listings

 - `get_superfamilies.py`: Retrieve the TCDB's superfamily list

 - `get_superfamily_sizes.py`: Counts structures and chains representing TCDB-defined superfamilies

 - `get_tcdb_children.py`: List all systems belonging to a TC-ID and its child nodes

 - `gev.py`: Generalized extreme value distribution viewer for arbitrary 1-dimensional numeric data

 - `localExtractFamily.pl`: Deprecated, used to run extractFamily.pl on locally downloaded all-TCDB FASTAs

 - `logarithm.py`: Print logarithms of numeric lines to stdout

 - `malign.py`: Align MEME motifs to other MEME motifs

 - `mast2fasta.py`: Extract MAST hits as FASTAs

 - `meme2fasta.py`: Extract MEME results as FASTAs

 - `mpsat.py`: Performs something like what GSAT does but with within-HMMTOP-state shuffles

 - `ortholog_census.py`: Counts up members of TC-subclasses?

 - `pdbavehas.py`: Plots average similarity data onto homologs with structures for visualization of conservation

 - `pepwheel.py`: Generalized pepwheel generator, supports multiple TMSs

 - `phoboshop.py`: GUI-based unwanted domain deleter

 - `plot_from_tsv.py`: Generates plots for 1-3 dimensional TSVs with numeric

 - `plot_hydro_from_opm.py`: Compares HMMTOP's TMS predictions to OPM TMS assignments

 - `proftmb_remote.py`: Deprecated, used to perform ProfTMB predictions on a machine capable of building and running ProfTMB

 - `proteins_are_lit.py`: Performs literature searches given a list of protein accessions

 - `revcomp.py`: Returns the reverse complement of a nucleotide sequence

 - `sample_fastas.py`: Draw samples from collections of sequence records with or without replacement

 - `shuffle_sequences_within_class.py`: Shuffles sequences or multiple sequence alignments, preserving HMMTOP-assigned states

 - `simplify_tcids.py`: Truncates TC-IDs

 - `smart.py`: TrimAl wrapper with rudimentary convergence detection

 - `tabulate_hhr.py`: Tabulate all vs. all domain-domain comparisons

 - `test_json.py`: Check if a JSON (or Gzipped JSON) file is valid

 - `tmtrainer.py`: Process manual TMS assignments and send them to a central repository

 - `tmweaver.py`: GUI-based TMS assignment. Also supports multiple alignments

 - `tsviss_army_knife.py`: General CLI TSV editing tool
