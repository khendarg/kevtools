#!/usr/bin/env python
from __future__ import unicode_literals, division

import kevtools_common.types
import urllib
import time

import re
import sys
import argparse

try: urlopen = urllib.urlopen
except AttributeError:
	import urllib.request
	urlopen = urllib.request.urlopen
 

class Stuff(object):

	tcids = kevtools_common.types.TCIDCollection()

	def fetch_all_tcids(self):
		url = 'http://www.tcdb.org/public/tcdb'
		tcstrings = []
		f = urlopen(url)
		n = 0
		last_t = time.time()

		#new way
		contents = f.read().decode('utf-8').split('\n')
		f.close()
		for l in contents:
			if l.startswith('>'):
				n += 1
				if not (n % 1000): 
					#print('{} headers scanned ({:0.2f}s since last message)'.format(n, time.time() - last_t))
					last_t = time.time()

				s = l[1:].strip()
				s = re.sub(' *\|', '|', s).split()[0]
				s = re.sub('\| *', '|', s).split()[0]
				s = re.sub('\.+', '.', s)
				splits = s.split('|')
				tc = splits[-1]
				acc = splits[-2]
				self.tcids.add_tcid('{}-{}'.format(tc, acc))

	def main(self, args):
		parser = argparse.ArgumentParser()

		parser.add_argument('-l', '--level', type=int, default=4, help='What TC-ID level to bring in (1: class, 2: subclass, 3: family, 4: subfamily, 5: system, 6: subunit')
		parser.add_argument('parents', metavar='parent_id', nargs='+', help='Which TC-IDs to fetch children for')

		params = parser.parse_args(args)

		self.fetch_all_tcids()

		selected = kevtools_common.types.TCIDCollection()

		for query in params.parents:
			found = self.tcids.search(query)
			for tcid in found: selected.add_tcid(tcid[:params.level])

		for tcid in sorted(selected): print(tcid)
		

		
if __name__ == '__main__':
	x = Stuff()
	x.main(sys.argv[1:])
