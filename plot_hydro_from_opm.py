#!/usr/bin/env python
import sys
import os
import argparse
import quod

import matplotlib
matplotlib.use('Qt4Agg')
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt

import Bio.PDB
try: CODE = Bio.PDB.protein_letters_3to1
except AttributeError: CODE = Bio.PDB.to_one_letter_code

def get_gapped_atomseq(f):
	sequences = {}
	for l in f:
		if l.startswith('ATOM') or l.startswith('HETATM'):
			chain = l[21]

			if chain not in sequences: sequences[chain] = {}

			try: resn = CODE[l[17:20]]
			except KeyError: continue
			resi = int(l[22:26].strip())

			sequences[chain][resi] = resn

	return sequences

class Stuff(object):
	tmdatadir = os.environ.get('TMDATA', None)

	def main(self, args):
		parser = argparse.ArgumentParser()

		if self.tmdatadir: parser.add_argument('--tmdatadir', default=self.tmdatadir, help='TMdata directory (default: $TMDATA, which == {})'.format(self.tmdatadir))
		else: parser.add_argument('--tmdatadir', default='tmdata', help='TMdata directory (default: ./tmdata ($TMDATA is unset)')

		parser.add_argument('pdbid', metavar='1PDB_C', help='PDB and chain to plot hydropathy for')
		#parser.add_argument('--pfam', action='store_true', help='Also try grabbing PFAM domains because whyever not')

		params = parser.parse_args(args)

		chain = 'A' if len(params.pdbid) < 6 else params.pdbid[5]

		seq = self.get_pdbseq(params.pdbid, chain)

		spans = self.get_opm_spans(params.pdbid)[chain]

		fig = plt.figure()

		plot = quod.Plot(fig=fig)
		plot.add(quod.Hydropathy(seq, style='b', window=19, index=quod.HYDROPATHY))
		plot.add(quod.Hydropathy(seq, style='r', window=19, index=quod.AMPHIPATHICITY))
		opm = quod.HMMTOP(seq)
		topspans = opm.spans
		opm.spans = spans
		plot.add(opm)
		for i, tms in enumerate(opm.spans):
			center = (tms[0] + tms[1]) / 2
			label = quod.Region(spans=[[center, center]], yspan=[1.0, 1.5], label='OPM\n{}'.format(i+1))
			plot.add(label)

		for i, tms in enumerate(topspans):
			hmmtop = quod.Region(spans=[tms], yspan=[-1.0, -0.5], label='HMMTOP\n{}'.format(i+1))
			plot.add(hmmtop)
		plot.render()
		plot.ax.set_title('{} (indices from OPM)'.format(params.pdbid))
		plt.show()

	def get_pdbseq(self, pdbid, chain='A'):
		if len(pdbid) >= 6:
			pdbacc = pdbid[:4]
			chain = pdbid[5]		
		else: pdbacc = pdbid

		seqdict = get_gapped_atomseq(open('{}/pdb/{}.pdb'.format(self.tmdatadir, pdbacc.lower())))
		seq = ''
		for i in range(1, max(seqdict[chain])):
			seq += seqdict[chain].get(i, 'X')
		return seq

	def get_opm_spans(self, pdbid):
		pdbacc = pdbid[:4]
		spandict = {}
		with open('{}/opm/ASSIGNMENTS.TSV'.format(self.tmdatadir)) as f:
			for l in f: 
				if l.startswith(pdbacc.lower()):
					spanstr = l.split('\t')[1]
					spans = [[int(x) for x in span.split('-')] for span in spanstr.split(',')]
					spandict[l[5]] = spans

		return spandict

if __name__ == '__main__':
	x = Stuff()
	x.main(sys.argv[1:])
