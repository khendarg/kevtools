#!/usr/bin/env python

import argparse
import json
import pathlib
import re
import sys

import matplotlib.pyplot as plt
import numpy as np
import scipy.cluster.hierarchy
import sklearn.cluster
from Bio import SeqIO

import anvio.dbops
from anvio.tables.collections import TablesForCollections
from anvio.tables.miscdata import MiscDataTableFactory
import anvio.utils

from kevtools_common.types import TCID
from kevtools_common.colors import colorhash

DEBUG = 1
FORCE_NAMES = 0

nongenomekeys = set([
    "blast", "TCID", "Acc", "CE", "Role", "hit_tms_no", 
    ])

#FIXME: horrible monkeypatching
if FORCE_NAMES:
    def is_this_name_OK_for_database(*args, **kwargs): return True
    anvio.utils.is_this_name_OK_for_database = is_this_name_OK_for_database

def info(*things):
    if DEBUG >= 1: print("[INFO]", *things, file=sys.stderr)

def precision_recall(inside, outside, level):
    tcids_inside = [tcid[:level] for tcid in inside]
    tcids_outside = [tcid[:level] for tcid in outside]

    counts_inside = {}
    for tcid in tcids_inside:
        if tcid not in counts_inside: counts_inside[tcid] = 0
        counts_inside[tcid] += 1
    counts_outside = {}
    for tcid in tcids_outside:
        if tcid not in counts_outside: counts_outside[tcid] = 0
        counts_outside[tcid] += 1

    countlist = list(zip(counts_inside.values(), counts_inside.keys()))
    countlist.sort(key=lambda x: -x[0])
    count, mode = countlist[0]

    precision = count / len(tcids_inside)
    recall = count / (count + counts_outside.get(mode, 0))
    return "cl_{}".format(str(mode).replace(".", "_").replace("-", "_")), precision, recall


def sanitize_superfamily(s):
    return _sanitize_superfamily(s).replace(" ", "_").replace("-", "_")

def _sanitize_superfamily(s):
    if re.match("^[-A-Za-z0-9/]+ [sS]uperfamily$", s) and FORCE_NAMES: return re.findall("^[-A-Za-z0-9]+", s)[0]
    elif re.match("^[-A-Za-z0-9]+ [sS]uperfamily$", s): return re.findall("^[-A-Za-z0-9]+", s)[0]
    elif re.match("Unclass", s): return "Unclassified"
    elif s.startswith("ABC"): return "ABC"
    elif s.startswith("Holin"): return s[:s.find(" Superfamily")]
    elif re.search("\([-A-Za-z0-9]+\)", s): return re.findall("\(([-A-Za-z0-9]+)\)", s)[0]
    elif re.search("\([-A-Za-z0-9/]+\)", s): 
        if FORCE_NAMES: return re.findall("\(([-A-Za-z0-9/]+)\)", s)[0]
        else: return re.findall("\(([-A-Za-z0-9/]+)\)", s)[0].replace("/", "-")
    elif "/" not in s: 
        if "Superfam" in s: return s[:s.find(" Superfam")]
        else: return s
    elif "/" in s and not FORCE_NAMES: return _sanitize_superfamily(s.replace("/", "-"))
    else: 
        raise ValueError("Unknown superfamily name pattern: {}".format(repr(s)))

def to_binary(bits):
    return sum([b << i for i, b in enumerate(bits[::-1])])

def to_gray(num):
    return num ^ (num >> 1)

def reverse_dict(d, relevant=None):
    out = {}
    for k in d:
        if relevant is not None and k not in relevant: continue
        if d[k] not in out: out[d[k]] = set()
        out[d[k]].add(str(k))
    return out

def parse_master_table(fh, parse_tcid=True, types=None):
    """
    types: if importing a table with nonstandard types, entries in types will override entries in fixedtypes or blasttypes
    """
    fields = None

    types = dict() if types is None else types
    fixedtypes = {
            "TCID": TCID,
            "Acc": str,
            "CE": str,
            "Role": str,
            "hit_tms_no": lambda x: int(float(x)),
            }
    if not parse_tcid: fixedtypes["TCID"] = str
    blasttypes = {
            "query": str,
            "q_tms": lambda x: int(float(x)) if x != "nan" else None,
            "evalue": float,
            "pident": float,
            "qcov": float,
            "scov": float,
            }

    varfields = []
    varfield = None
    outdict = {}
    for l in fh:
        varfieldindex = None
        sl = l[1:-1].split("\t") if l.startswith("#") else l[:-1].split("\t")

        if fields is None: 
            fields = sl
            for k in fields:
                if k in fixedtypes: pass
                elif k in blasttypes: pass
                elif k in types: pass
                else: varfields.append(k)

        elif sl:
            k = TCID(sl[0]) if parse_tcid else sl[0]
            outdict[k] = row = dict(blast=dict())
            for field, value in zip(fields, sl):
                if field in types:
                    row[field] = types[field](value)
                elif field in fixedtypes:
                    row[field] = fixedtypes[field](value)
                elif field in blasttypes:

                    if field == "query":
                        if varfieldindex is None: varfieldindex = 0
                        else: varfieldindex += 1
                        varfield = varfields[varfieldindex]

                        if value == "none":
                            record = False
                        else:
                            record = True
                            row["blast"][varfield] = dict(query=blasttypes[field](value))
                    #if value == "none": continue
                    elif record:
                        try: row["blast"][varfield][field] = blasttypes[field](value)
                        except ValueError: row["blast"][varfield][field] = None
                elif field in varfields:
                    row[field] = 1 if value == "+" else 0 if value == "-" else value
    return outdict, varfields

def filter_master_table(table, varfields, allow=None, deny=None):
    if allow is None and deny is None: return table

    newtable = {}
    for tcid in table:
        row = {}
        seen = False
        for field in table[tcid]:
            if field in varfields: #genome field
                state = table[tcid][field]
                if allow and table[tcid]["blast"].get(field, {}).get("query") not in allow:
                    state &= 0
                if deny and table[tcid]["blast"].get(field, {}).get("query") in deny:
                    state &= 0
                row[field] = state
                if state > 0: seen = True
            else: row[field] = table[tcid][field]
        if seen: newtable[tcid] = row

    return newtable

def pool_master_table(table, level, method="presence"):
    for k in table:
        startlevel = len(TCID(k))
        break
    if startlevel == level: return table
    
    newtable = {}
    genomekeys = set()
    order = []
    for itemname in table:
        newitemname = TCID(itemname)[:level]
        if newitemname not in newtable: newtable[newitemname] = dict()

        #print(table[itemname])
        #for key in table[itemname]["blast"]:
        #    genomekeys.add(key)

        for k in table[itemname]:
            if k in nongenomekeys: continue
            if k not in newtable[newitemname]: 
                newtable[newitemname][k] = 1 if method == "uniformity" else 0

            if method == "presence":
                newtable[newitemname][k] |= table[itemname][k]
            elif method == "count":
                newtable[newitemname][k] += table[itemname][k]
            elif method == "uniformity":
                newtable[newitemname][k] &= table[itemname][k]
            else: raise ValueError("Unknown pooling method: {}".format(repr(method)))


    #tmptable = dict(zip([str(_) for _ in newtable], [newtable[_] for _ in newtable]))
    #print(json.dumps(tmptable, indent=1))
    return newtable

def parse_superfamilies(fh):
    fields = []
    to_family = {}
    to_superfamily = {}
    to_sane_superfamily = {}
    for l in fh:
        if l.startswith("#"): fields = l[1:-1].split("\t")
        elif l.startswith("#"): continue
        elif not l.strip(): continue
        else:
            sl = l[:-1].split("\t")
            for k in sl[:3]:
                to_family[k] = sl[3]
                to_superfamily[k] = sl[4]
                to_sane_superfamily[k] = sanitize_superfamily(sl[4])

    return to_family, to_superfamily, to_sane_superfamily

def assign_superfamilies(tciterable, sfdict, threshold=0):
    sfcounts = {}
    mapping = {}
    for tcid in tciterable:
        sf = sfdict.get(tcid, sfdict.get(str(tcid), "Unclassified"))
        if sf in sfcounts: sfcounts[sf] += 1
        else: sfcounts[sf] = 1
        mapping[tcid] = sf

    if threshold > 0:
        for tcid in mapping:
            if sfcounts[mapping[tcid]] <= threshold: mapping[tcid] = "Various_SFs"

    return mapping

def assign_colors(siterable, salt=""):
    out = {}
    for s in siterable: out[s] = colorhash(s, outfmt="hex", salt=salt)
    return out

def compute_layer_data(genomes, mastertable):
    out = {}
    for genome in genomes: out[genome] = dict(split_name=genome, transportome_size=0)
    layerfields = ["split_name", "transportome_size"]
    subclassfields = set()
    classfields = set()
    for _ in genomes:
        level = len(_)
        break
    for tcid in mastertable:
        for genome in genomes:
            out[genome]["transportome_size"] += mastertable[tcid][genome]
            if level >= 1:
                k = "classes!{}".format(tcid[:1])
                classfields.add(k)
                if mastertable[tcid][genome]:
                    if k in out[genome]: out[genome][k] += 1
                    else: out[genome][k] = 1
            if level >= 2:
                k = "subclasses!{}".format(tcid[:2])
                subclassfields.add(k)
                if mastertable[tcid][genome]:
                    if k in out[genome]: out[genome][k] += 1
                    else: out[genome][k] = 1
    for genome in genomes:
        for k in sorted(subclassfields):
            if k not in out[genome]: out[genome][k] = 0
        for k in sorted(classfields):
            if k not in out[genome]: out[genome][k] = 0
    return out, sorted(layerfields) + sorted(classfields) + sorted(subclassfields)
        
def parse_layer_data(fh):
    fields = None
    data = {}
    for l in fh:
        sl = l[:-1].split("\t")
        if fields is None: fields = sl
        elif l.strip():
            data[sl[0]] = dict(zip(fields[1:], sl[1:]))
    return data, fields[1:]

def generate_itemorders(mastertable, to_superfamily, genomes):
    orders = {}

    sfsizes = {}
    for tcid in to_superfamily:
        try: sfsizes[to_superfamily[tcid]] += 1
        except KeyError: sfsizes[to_superfamily[tcid]] = 1

    #1. SF size - num contrib genomes - gray code repr
    sfcount_src = list(zip(
            mastertable,
            [sfsizes[to_superfamily[tcid]] for tcid in mastertable],
            [to_superfamily[tcid] for tcid in mastertable],
            [sum([mastertable[tcid][genome] for genome in genomes]) for tcid in mastertable],
            [to_gray(to_binary([mastertable[tcid][genome] for genome in genomes])) for tcid in mastertable],
            ))
    sfcount_src.sort(key=lambda x: (x[1:], x[0]))
    orders["sfcount_gray"] = [_[0] for _ in sfcount_src]

    #2. 
    return orders

def parse_layerorder(fn):
    if fn.suffix in (".nwk", ".phy", ".tree"):
        with open(fn) as fh: s = fh.read().replace("\n", "").replace("'", "")
        name = fn.stem
        return dict(
                data_type="newick",
                data_value=s,
                )
    else:
        with open(fn) as fh:
            sl = [_ for _ in fh]
            #probably already anvio-friendly
            if len(sl) == 1:
                s = sl[0].replace("\n", "")
            elif len(sl) == 0:
                raise ValueError("Is your layer order file {} empty?".format(fn))
            else:
                s = ",".join(sl).replace("\n", "")
            return dict(
                    data_type="basic",
                    data_value=s,
                    )

def main(args):
    args.outdir.mkdir(exist_ok=True)

    info("Creating profile DB")

    if args.profile_db is None:
        profile_db = args.outdir / "profile.db"
    if profile_db.is_file(): 
        #FIXME: Don't just rewrite existing profile DBs
        profile_db.unlink()

    title = "Ad hoc display" if args.title is None else args.title

    profiledb = anvio.dbops.ProfileDatabase(str(profile_db))
    profiledb.create(dict(
        db_type="profile",
        db_variant="ad_hoc_display",
        blank=True,
        merged=True,
        contigs_db_hash=None,
        items_ordered=False,
        samples=", ".join(["layer1", "layer2"]), #ITEMDATA FIELDS
        sample_id=title))

    allowlist = None
    denylist = None

    for fn in args.allow:
        if allowlist is None: allowlist = set()
        with open(fn) as fh:
            allowlist.update([_.replace("\n", "") for _ in fh])
    for fn in args.deny:
        if denylist is None: denylist = set()
        with open(fn) as fh:
            denylist.update([_.replace("\n", "") for _ in fh])

    with open(args.mastertable) as fh:
        mastertable, genomefields = parse_master_table(fh, parse_tcid=False)
        mastertable = filter_master_table(mastertable, genomefields, allowlist, denylist)
        pooledtable = pool_master_table(mastertable, level=args.level, method=args.pooling_method)

    with open(args.outdir/"itemdata.tsv", "w") as fh:
        fh.write("item_name\t" + "\t".join(genomefields) + "\n")
        for itemname in pooledtable:
            fh.write("{}\t{}\n".format(itemname, "\t".join([str(pooledtable[itemname][_]) for _ in pooledtable[itemname]])))


    #import superfamilies
    if args.superfamilies is None: raise NotImplementedError("Online superfamilies lookup is not implemented yet")
    else:
        with open(args.superfamilies) as fh:
            to_families, to_superfamilies, to_sanitized = parse_superfamilies(fh)

    superfamilies = assign_superfamilies(pooledtable, to_sanitized, 0)
    with open(args.outdir / "collection.tsv", "w") as fh:
        for tcid in sorted(superfamilies):
            fh.write("{}\t{}\n".format(tcid, superfamilies[tcid]))
    reducedsuperfamilies = assign_superfamilies(pooledtable, to_sanitized, args.small_sf)
    with open(args.outdir / "collection_reduced.tsv", "w") as fh:
        for tcid in sorted(reducedsuperfamilies):
            fh.write("{}\t{}\n".format(tcid, reducedsuperfamilies[tcid]))

    #generate bin colors
    superfamcolors = assign_colors(sorted(set(to_sanitized.values())), salt="puma")
    superfamcolors["Unclassified"] = colorhash("Unclassified", outfmt="hex", salt="puma")
    superfamcolors["Various_SFs"] = colorhash("Various_SFs", outfmt="hex", salt="puma")
    with open(args.outdir / "collectioninfo.tsv", "w") as fh:
        for sfname in superfamcolors: 
            fh.write("{}\tpuma\t{}\n".format(sfname, superfamcolors[sfname]))

    layerdata, layerfields = compute_layer_data(genomefields, pooledtable)
    #import layer info
    if args.layer_data:
        with open(args.layer_data) as fh:
            newlayerdata, newlayerfields = parse_layer_data(fh)
        layerfields.extend(newlayerfields)
        for genome in layerdata:
            layerdata[genome].update(newlayerdata[genome])

    with open(args.outdir / "layers.tsv", "w") as fh:
        fh.write("\t".join(str(_) for _ in layerfields) + "\n")
        for genome in layerdata:
            fh.write("\t".join(str(layerdata[genome][field]) for field in layerdata[genome]) + "\n")

    itemorders = generate_itemorders(pooledtable, superfamilies, genomefields)
    for order in itemorders:
        with open(args.outdir / "itemorder_{}".format(order), "w") as fh:
            fh.write("\n".join([str(_) for _ in itemorders[order]]))

    layerorders = {}
    for fn in args.layer_order:
        layerorders[fn.stem] = parse_layerorder(fn)
    if layerorders:
        with open(args.outdir / "layer_orders.tsv", "w") as fh:
            fh.write("item_name\tdata_type\tdata_value\n")
            for ordername in layerorders:
                fh.write("{}\t{}\t{}\n".format(ordername, layerorders[ordername]["data_type"], layerorders[ordername]["data_value"]))
    
    ########################################
    #put stuff in the DB
    ########################################
    #MiscDataTableFactory(argparse.Namespace(
    #    contigs_db=None,
    #    data_file=str(args.outdir/"itemdata.tsv"),
    #    just_do_it=False,
    #    pan_or_profile_db=str(profile_db),
    #    target_data_group=None,
    #    target_data_table="items",
    #    transpose=False,
    #    )).populate_from_file(str(args.outdir/"itemdata.tsv"))

    MiscDataTableFactory(argparse.Namespace(
        contigs_db=None,
        data_file=str(args.outdir/"layers.tsv"),
        just_do_it=False,
        pan_or_profile_db=str(profile_db),
        target_data_group=None,
        target_data_table="layers",
        transpose=False,
        )).populate_from_file(str(args.outdir/"layers.tsv"))

    if layerorders:
        MiscDataTableFactory(argparse.Namespace(
            contigs_db=None,
            data_file=str(args.outdir/"layer_orders.tsv"),
            just_do_it=False,
            pan_or_profile_db=str(profile_db),
            target_data_group=None,
            target_data_table="layer_orders",
            transpose=False,
            )).populate_from_file(str(args.outdir/"layer_orders.tsv"))

    #create collections table
    collections = TablesForCollections(str(profile_db))
    revdict = reverse_dict(superfamilies, pooledtable)
    collections.append("Superfamilies_full", revdict, dict(zip(revdict, [dict(source="puma", html_color=superfamcolors[sf]) for sf in revdict])))
    revdict_reduced = reverse_dict(reducedsuperfamilies, pooledtable)
    collections.append("Superfamilies_reduced", revdict_reduced, dict(zip(revdict_reduced, [dict(source="puma", html_color=superfamcolors[sf]) for sf in revdict_reduced])))

    with open(args.outdir/"run_interactive.sh", "w") as fh:
        fh.write("anvi-interactive -p {profile_db} --manual -d {itemdata} --items-order {itemorder} --title \"{title}\"\n".format(
            profile_db=profile_db,
            itemdata=args.outdir / "itemdata.tsv",
            itemorder=args.outdir/ "itemorder_sfcount_gray",
            title=title,
            ))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("mastertable", type=pathlib.Path, 
            help="Master table with TCID, accession, ChEbi ID, role, count data, per-genome presence-absence data, and additional query, query TMS, E-value, %ID, query coverage, and subject coverage data for each genome")

    parser.add_argument("-p", "--profile-db", type=pathlib.Path,
            help="Anvi'o profile database to edit/create. Defaults to outdir/profile.db if not set")

    parser.add_argument("-o", "--outdir", type=pathlib.Path, required=True,
            help="Output directory")


    parser.add_argument("--layer-data", type=pathlib.Path,
            help="Additional layer data to join. Typically whole-genome/species-related, e.g. genome size, GC-content, phenotype data")

    parser.add_argument("--level", type=int, default=4, 
            help="TC-level to pool results at. (0: Superfamily, 1: TC-class, 2: TC-subclass, 3: TC-family, 4: TC-subfamily, 5: TC-system, 6: TC-component")

    parser.add_argument("--superfamilies", type=pathlib.Path,
            help="If set, a local copy of the superfamilies table from https://tcdb.org/cgi-bin/substrates/listSuperfamilies.py")


    parser.add_argument("--small-sf", type=int, default=3, 
            help="Maximum size for 'small' superfamilies merged into the 'Various SFs' pseudofamily")

    parser.add_argument("--title",
            help="Plot title")

    parser.add_argument("--layer-order", type=pathlib.Path, nargs="+", default=[],
            help="Layer order files. .nwk, .phy, and .tree files will be parsed as trees, and anything else will be parsed as simple orders")

    parser.add_argument("--pooling-method", default="presence",
            help="Method to use for pooling specific TCIDs. (presence (default), uniformity, count)")

    parser.add_argument("--allow", type=pathlib.Path, nargs="+", default=[],
            help="List of BLAST accessions to display")
    parser.add_argument("--deny", type=pathlib.Path, nargs="+", default=[],
            help="List of BLAST accessions to display")
    args = parser.parse_args()

    main(args)
